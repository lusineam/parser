const oncologyIndustryTrialCountries =
  require("../modules/trialResults/oncologyIndustryTrialCountries").oncologyIndustryTrialCountries;
const oncologyIndustryTrialDrugsTested_and_Sponsor =
  require("../modules/trialResults/oncologyIndustryTrialDrugsTested_and_Sponsor").oncologyIndustryTrialDrugsTested_and_Sponsor;
const oncologyIndustryTrialDrugsTested =
  require("../modules/trialResults/oncologyIndustryTrialDrugsTested").oncologyIndustryTrialDrugsTested;
const oncologyIndustryTrialPanel =
  require("../modules/trialResults/oncologyIndustryTrialPanel").oncologyIndustryTrialPanel;
const oncologyIndustryTrialPrimaryEndpoint =
  require("../modules/trialResults/oncologyIndustryTrialPrimaryEndpoint").oncologyIndustryTrialPrimaryEndpoint;
const oncologyIndustryTrialProtocolIDs =
  require("../modules/trialResults/oncologyIndustryTrialProtocolIDs").oncologyIndustryTrialProtocolIDs;
const oncologyIndustryTrialSponsors =
  require("../modules/trialResults/oncologyIndustryTrialSponsors").oncologyIndustryTrialSponsors;
const oncologyIndustryTrialTags =
  require("../modules/trialResults/oncologyIndustryTrialTags").oncologyIndustryTrialTags;
const oncologyIndustryTrialTherapeuticAreas =
  require("../modules/trialResults/oncologyIndustryTrialTherapeuticAreas").oncologyIndustryTrialTherapeuticAreas;
const oncologyIndustryTrialTiming =
  require("../modules/trialResults/oncologyIndustryTrialTiming").oncologyIndustryTrialTiming;
const oncologyLineSelectionTrials =
  require("../modules/trialResults/oncologyLineSelectionTrials").oncologyLineSelectionTrials;
const oncologyNSCLCPivotalEventMilestones =
  require("../modules/trialResults/oncologyNSCLCPivotalEventMilestones").oncologyNSCLCPivotalEventMilestones;
const oncologyNSCLCPivotalPatients =
  require("../modules/trialResults/oncologyNSCLCPivotalPatients").oncologyNSCLCPivotalPatients;
const oncologyNSCLCPivotalResultsScatter =
  require("../modules/trialResults/oncologyNSCLCPivotalResultsScatter").oncologyNSCLCPivotalResultsScatter;
const oncologyNSCLCPivotalResults =
  require("../modules/trialResults/oncologyNSCLCPivotalResults").oncologyNSCLCPivotalResults;
const oncologyOtherFeaturesSelectionTrials =
  require("../modules/trialResults/oncologyOtherFeaturesSelectionTrials").oncologyOtherFeaturesSelectionTrials;
const oncologyStageSelectionTrials =
  require("../modules/trialResults/oncologyStageSelectionTrials").oncologyStageSelectionTrials;

let functions = [
  oncologyIndustryTrialCountries,
  oncologyIndustryTrialDrugsTested,
  oncologyIndustryTrialDrugsTested_and_Sponsor,
  oncologyIndustryTrialPanel,
  oncologyIndustryTrialPrimaryEndpoint,
  oncologyIndustryTrialProtocolIDs,
  oncologyIndustryTrialSponsors,
  oncologyIndustryTrialTags,
  oncologyIndustryTrialTherapeuticAreas,
  oncologyIndustryTrialTiming,
  oncologyLineSelectionTrials,
  oncologyNSCLCPivotalEventMilestones,
  oncologyNSCLCPivotalPatients,
  oncologyNSCLCPivotalResults,
  oncologyNSCLCPivotalResultsScatter,
  oncologyOtherFeaturesSelectionTrials,
  oncologyStageSelectionTrials,
];

functions.map(async (func) => {
  await func();
});
