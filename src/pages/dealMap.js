const dealMapDealCompany =
  require("../modules/dealMap/dealMapDealCompany").dealMapDealCompany;
const dealMapDealPanel =
  require("../modules/dealMap/dealMapDealPanel").dealMapDealPanel;
const dealMapDealType =
  require("../modules/dealMap/dealMapDealType").dealMapDealType;
const dealMapDealProduct =
  require("../modules/dealMap/dealMapDealProduct").dealMapDealProduct;
const dealMapDrugMechanismOfAction =
  require("../modules/dealMap/dealMapDrugMechanismOfAction").dealMapDrugMechanismOfAction;
const dealMapDrugModality =
  require("../modules/dealMap/dealMapDrugModality").dealMapDrugModality;
const dealMapDrugTargets =
  require("../modules/dealMap/dealMapDrugTargets").dealMapDrugTargets;

let functions = [
  dealMapDealCompany,
  dealMapDealPanel,
  dealMapDealProduct,
  dealMapDealType,
  dealMapDrugMechanismOfAction,
  dealMapDrugModality,
  dealMapDrugTargets,
];

functions.map(async (func) => {
  await func();
});
