const clinOpsDrugMechanismOfAction =
  require("../modules/clinOps/clinOpsDrugMechanismOfAction").clinOpsDrugMechanismOfAction;
const clinOpsIndustryTrialCountries =
  require("../modules/clinOps/clinOpsIndustryTrialCountries").clinOpsIndustryTrialCountries;
const clinOpsIndustryTrialDrugsTested =
  require("../modules/clinOps/clinOpsIndustryTrialDrugsTested").clinOpsIndustryTrialDrugsTested;
const clinOpsIndustryTrialPanel =
  require("../modules/clinOps/clinOpsIndustryTrialPanel").clinOpsIndustryTrialPanel;
const clinOpsIndustryTrialPrimaryEndpoint =
  require("../modules/clinOps/clinOpsIndustryTrialPrimaryEndpoint").clinOpsIndustryTrialPrimaryEndpoint;
const clinOpsIndustryTrialProtocolIDs =
  require("../modules/clinOps/clinOpsIndustryTrialProtocolIDs").clinOpsIndustryTrialProtocolIDs;
const clinOpsIndustryTrialSponsors =
  require("../modules/clinOps/clinOpsIndustryTrialSponsors").clinOpsIndustryTrialSponsors;
const clinOpsIndustryTrialTherapeuticAreas =
  require("../modules/clinOps/clinOpsIndustryTrialTherapeuticAreas").clinOpsIndustryTrialTherapeuticAreas;
const clinOpsInvestigatorDiseaseTiers =
  require("../modules/clinOps/clinOpsInvestigatorDiseaseTiers").clinOpsInvestigatorDiseaseTiers;
const clinOpsInvestigatorPanel =
  require("../modules/clinOps/clinOpsInvestigatorPanel").clinOpsInvestigatorPanel;
const clinOpsLineSelectionTrials =
  require("../modules/clinOps/clinOpsLineSelectionTrials").clinOpsLineSelectionTrials;
const clinOpsOrganizationGeoLocation =
  require("../modules/clinOps/clinOpsOrganizationGeoLocation").clinOpsOrganizationGeoLocation;
const clinOpsOrganizationPanel =
  require("../modules/clinOps/clinOpsOrganizationPanel").clinOpsOrganizationPanel;
const clinOpsOrganizationTrialInvestigators =
  require("../modules/clinOps/clinOpsOrganizationTrialInvestigators").clinOpsOrganizationTrialInvestigators;
const clinOpsOrganizationTrials =
  require("../modules/clinOps/clinOpsOrganizationTrials").clinOpsOrganizationTrials;
const clinOpsOtherFeaturesSelectionTrials =
  require("../modules/clinOps/clinOpsOtherFeaturesSelectionTrials").clinOpsOtherFeaturesSelectionTrials;
const clinOpsStageSelectionTrials =
  require("../modules/clinOps/clinOpsStageSelectionTrials").clinOpsStageSelectionTrials;
const clinOpsIndustryNewTrialPanel =
  require("../modules/clinOps/clinOpsIndustryNewTrialPanel").clinOpsIndustryNewTrialPanel;
const clinOpsNewLocation =
  require("../modules/clinOps/clinOpsNewLocation").clinOpsNewLocation;

let functions = [
  clinOpsDrugMechanismOfAction,
  clinOpsIndustryNewTrialPanel,
  clinOpsIndustryTrialCountries,
  clinOpsIndustryTrialDrugsTested,
  clinOpsIndustryTrialPanel,
  clinOpsIndustryTrialPrimaryEndpoint,
  clinOpsIndustryTrialProtocolIDs,
  clinOpsIndustryTrialSponsors,
  clinOpsIndustryTrialTherapeuticAreas,
  clinOpsInvestigatorDiseaseTiers,
  clinOpsInvestigatorPanel,
  clinOpsLineSelectionTrials,
  clinOpsNewLocation,
  clinOpsOrganizationGeoLocation,
  clinOpsOrganizationPanel,
  clinOpsOtherFeaturesSelectionTrials,
  clinOpsStageSelectionTrials,
];

functions.map(async (func) => {
  await func();
});

clinOpsOrganizationTrialInvestigators();
clinOpsOrganizationTrials();
