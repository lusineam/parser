const drugMapDrugPanel =
  require("../modules/drugMap/drugMapDrugPanel").drugMapDrugPanel;
const drugMapDrugProgramPanel =
  require("../modules/drugMap/drugMapDrugProgramPanel").drugMapDrugProgramPanel;
const drugMapIndustryTrialDrugsTested =
  require("../modules/drugMap/drugMapIndustryTrialDrugsTested").drugMapIndustryTrialDrugsTested;
const drugMapIndustryTrialPanel =
  require("../modules/drugMap/drugMapIndustryTrialPanel").drugMapIndustryTrialPanel;
const drugMapIndustryTrialSponsors =
  require("../modules/drugMap/drugMapIndustryTrialSponsors").drugMapIndustryTrialSponsors;
const drugMapIndustryTrialTherapeuticAreas =
  require("../modules/drugMap/drugMapIndustryTrialTherapeuticAreas").drugMapIndustryTrialTherapeuticAreas;
const drugMapIndustryTrialTiming =
  require("../modules/drugMap/drugMapIndustryTrialTiming").drugMapIndustryTrialTiming;
const drugMapMOA = require("../modules/drugMap/drugMapMOA").drugMapMOA;

let functions = [
  drugMapDrugPanel,
  drugMapDrugProgramPanel,
  drugMapIndustryTrialDrugsTested,
  drugMapIndustryTrialTherapeuticAreas,
  drugMapIndustryTrialPanel,
  drugMapIndustryTrialSponsors,
  drugMapIndustryTrialTiming,
  drugMapMOA,
];

functions.map(async (func) => {
  await func();
});
