const trialDrugMechanismOfAction =
  require("../modules/trials/trialDrugMechanismOfAction").trialDrugMechanismOfAction;
const trialDrugModality =
  require("../modules/trials/trialDrugModality").trialDrugModality;
const trialIndustryCountries =
  require("../modules/trials/trialIndustryCountries").trialIndustryCountries;
const trialIndustryNewTherapeuticAreas =
  require("../modules/trials/trialIndustryNewTherapeuticAreas").trialIndustryNewTherapeuticAreas;
const trialIndustryPanel =
  require("../modules/trials/trialIndustryPanel").trialIndustryPanel;
const trialIndustrySponsor =
  require("../modules/trials/trialIndustrySponsor").trialIndustrySponsor;
const trialIndustryTherapeuticAreas =
  require("../modules/trials/trialIndustryTherapeuticAreas").trialIndustryTherapeuticAreas;
const trialIndustryTrialDrugsTested =
  require("../modules/trials/trialIndustryTrialDrugsTested").trialIndustryTrialDrugsTested;
const trialIndustryTrialDrugsTestedAndSponsor =
  require("../modules/trials/trialIndustryTrialDrugsTestedAndSponsor").trialIndustryTrialDrugsTestedAndSponsor;
const trialIndustryTrialPrimaryEndpoint =
  require("../modules/trials/trialIndustryTrialPrimaryEndpoint").trialIndustryTrialPrimaryEndpoint;
const trialIndustryTrialProtocolIDs =
  require("../modules/trials/trialIndustryTrialProtocolIDs").trialIndustryTrialProtocolIDs;
const trialIndustryTrialTags =
  require("../modules/trials/trialIndustryTrialTags").trialIndustryTrialTags;
const trialIndustryTrialTiming =
  require("../modules/trials/trialIndustryTrialTiming").trialIndustryTrialTiming;
const trialLineSelectionTrials =
  require("../modules/trials/trialLineSelectionTrials").trialLineSelectionTrials;
const trialIndustryTrialTimingMilestone =
  require("../modules/trials/trialIndustryTrialTimingMilestone").trialIndustryTrialTimingMilestone;
const trialOtherFeaturesSelectionTrials =
  require("../modules/trials/trialOtherFeaturesSelectionTrials").trialOtherFeaturesSelectionTrials;
const trialPhaseStatusDate =
  require("../modules/trials/trialPhaseStatusDate").trialPhaseStatusDate;
const trialStageSelectionTrials =
  require("../modules/trials/trialStageSelectionTrials").trialStageSelectionTrials;

let functions = [
  trialDrugMechanismOfAction,
  trialDrugModality,
  trialIndustryCountries,
  trialIndustryPanel,
  trialIndustrySponsor,
  trialIndustryTrialDrugsTested,
  trialIndustryTrialDrugsTestedAndSponsor,
];

let functions2 = [
  trialIndustryTrialPrimaryEndpoint,
  trialIndustryTrialProtocolIDs,
  trialIndustryTrialTags,
  trialIndustryTrialTiming,
  trialIndustryTrialTimingMilestone,
  trialLineSelectionTrials,
  trialOtherFeaturesSelectionTrials,
  trialPhaseStatusDate,
  trialStageSelectionTrials,
];

functions.map(async (func) => {
  await func();
});

functions2.map(async (func) => {
  await func();
});

trialIndustryNewTherapeuticAreas();
trialIndustryTherapeuticAreas();
