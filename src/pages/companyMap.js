const bloombergDrugMechanismOfAction =
  require("../modules/companyMap/bloombergDrugMechanismOfAction").bloombergDrugMechanismOfAction;
const bloombergDrugModality =
  require("../modules/companyMap/bloombergDrugModality").bloombergDrugModality;
const bloombergDrugPanel =
  require("../modules/companyMap/bloombergDrugPanel").bloombergDrugPanel;
const bloombergDrugProgramPanel =
  require("../modules/companyMap/bloombergDrugProgramPanel").bloombergDrugProgramPanel;
const bloombergIndustryTrialDrugsTested =
  require("../modules/companyMap/bloombergIndustryTrialDrugsTested").bloombergIndustryTrialDrugsTested;
const bloombergIndustryTrialDrugsTestedAndSponsor =
  require("../modules/companyMap/bloombergIndustryTrialDrugsTestedAndSponsor").bloombergIndustryTrialDrugsTestedAndSponsor;
const bloombergIndustryTrialOutcomes =
  require("../modules/companyMap/bloombergIndustryTrialOutcomes").bloombergIndustryTrialOutcomes;
const bloombergIndustryTrialPanel =
  require("../modules/companyMap/bloombergIndustryTrialPanel").bloombergIndustryTrialPanel;
const bloombergIndustryTrialProtocolIDs =
  require("../modules/companyMap/bloombergIndustryTrialProtocolIDs").bloombergIndustryTrialProtocolIDs;
const bloombergIndustryTrialSponsors =
  require("../modules/companyMap/bloombergIndustryTrialSponsors").bloombergIndustryTrialSponsors;
const bloombergIndustryTrialTherapeuticAreas =
  require("../modules/companyMap/bloombergIndustryTrialTherapeuticAreas").bloombergIndustryTrialTherapeuticAreas;
const bloombergIndustryTrialTiming =
  require("../modules/companyMap/bloombergIndustryTrialTiming").bloombergIndustryTrialTiming;
const bloombergOrganizationGeoLocation =
  require("../modules/companyMap/bloombergOrganizationGeoLocation").bloombergOrganizationGeoLocation;
const bloombergOrganizationPanel =
  require("../modules/companyMap/bloombergOrganizationPanel").bloombergOrganizationPanel;
const bloombergOrganizationTrials =
  require("../modules/companyMap/bloombergOrganizationTrials").bloombergOrganizationTrials;
const bloombergStatusOrder =
  require("../modules/companyMap/bloombergStatusOrder").bloombergStatusOrder;
const bloombergTherapeuticAreas =
  require("../modules/companyMap/bloombergTherapeuticAreas").bloombergTherapeuticAreas;
const bloombergFiltersData =
  require("../modules/companyMap/bloombergFiltersData").bloombergFiltersData;

let functions = [
  bloombergDrugMechanismOfAction,
  bloombergDrugModality,
  bloombergDrugPanel,
  bloombergDrugProgramPanel,
  bloombergFiltersData,
  bloombergIndustryTrialDrugsTested,
  bloombergIndustryTrialDrugsTestedAndSponsor,
  bloombergIndustryTrialOutcomes,
  bloombergIndustryTrialPanel,
];
let functions2 = [
  bloombergIndustryTrialProtocolIDs,
  bloombergIndustryTrialSponsors,
  bloombergIndustryTrialTherapeuticAreas,
  bloombergIndustryTrialTiming,
  bloombergOrganizationGeoLocation,
  bloombergOrganizationPanel,
  bloombergOrganizationTrials,
  bloombergStatusOrder,
  bloombergTherapeuticAreas,
];

functions.map(async (func) => {
  await func();
});

functions2.map(async (func) => {
  await func();
});
