const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function drugMapIndustryTrialDrugsTested () {
    readCsvFile("../dataCsv/drugMap/industryTrialDrugsTested.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let drugId = csvData[i]["drugId"];
          let drugNameId = csvData[i]["drugNameId"];
          let drugName = csvData[i]["drugName"];
          let drugList = csvData[i]["drugList"];
          
          const queryString = `INSERT INTO "drugMapIndustryTrialDrugsTested" ("trialId", "drugId", "drugNameId", "drugName", "drugList")
          VALUES ($1, $2, $3, $4, $5)`;
          
          db.any(queryString, [trialId, drugId, drugNameId, drugName, drugList])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }

exports.drugMapIndustryTrialDrugsTested = drugMapIndustryTrialDrugsTested;