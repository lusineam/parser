const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function drugMapMOA () {
    readCsvFile("../dataCsv/drugMap/drugMoA.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let HRCHY1 = csvData[i]["level_1"];
          let HRCHY2 = csvData[i]["level_2"];
          let HRCHY3 = csvData[i]["level_3"];
          let HRCHY4 = csvData[i]["level_4"];
          let HRCHY5 = csvData[i]["level_5"];
          let HRCHY6 = csvData[i]["level_6"];
          
          const queryString = `INSERT INTO "drugMapMOA" ("drugId", "HRCHY1", "HRCHY2", "HRCHY3", "HRCHY4", "HRCHY5",  "HRCHY6") VALUES ($1, $2, $3, $4, $5, $6, $7)`;
          
          db.any(queryString, [drugId, HRCHY1, HRCHY2, HRCHY3, HRCHY4, HRCHY5, HRCHY6])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }

exports.drugMapMOA = drugMapMOA;


