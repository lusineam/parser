const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function drugMapIndustryTrialTiming() {
  readCsvFile("../dataCsv/drugMap/industryTrialTiming.csv")
    .then((csvData) => {
      for (let i in csvData) {
        let trialId = csvData[i]["trialId"];
        let trialStartDate = csvData[i]["trialStartDate"];
        let trialEndDate = csvData[i]["trialEndDate"];
        let trialEnrollmentEndDate = csvData[i]["trialEnrollmentEndDate"];
        let durationPct = csvData[i]["durationPct"];

        if (trialStartDate === "") {
          trialStartDate = null;
        }
        if (trialEndDate === "") {
          trialEndDate = null;
        }
        if (trialEnrollmentEndDate === "") {
          trialEnrollmentEndDate = null;
        }

        const queryString = `INSERT INTO "drugMapIndustryTrialTiming" ("trialId", "trialStartDate", "trialEndDate", "trialEnrollmentEndDate", "durationPct") VALUES ($1, $2, $3, $4, $5)`;

        db.any(queryString, [
          trialId,
          trialStartDate,
          trialEndDate,
          trialEnrollmentEndDate,
          durationPct,
        ])
          .then((data) => {})
          .catch((error) => {
            console.log(error, data);
          });
      }
    })
    .catch((error) => {
      console.log(error);
    });
}

exports.drugMapIndustryTrialTiming = drugMapIndustryTrialTiming;
