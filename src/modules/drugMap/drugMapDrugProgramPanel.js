const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function drugMapDrugProgramPanel () {
    readCsvFile("../dataCsv/drugMap/drugProgramPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let drugPrimaryName = csvData[i]["drugPrimaryName"];
          let globalStatus = csvData[i]["globalStatus"];
          let countryName = csvData[i]["countryName"];
          let diseaseName = csvData[i]["diseaseName"];
          let currentStatus = csvData[i]["currentStatus"];
          let therapeuticArea = csvData[i]["therapeuticArea"];
          let companyRole = csvData[i]["companyRole"];
          let drugTherapeuticModality = csvData[i]["drugTherapeuticModality"];
          
          const queryString = `INSERT INTO "drugMapDrugProgramPanel" ("drugId", "drugPrimaryName", "globalStatus", "countryName", "diseaseName", "currentStatus",  "therapeuticArea", "companyRole", 
          "drugTherapeuticModality")
          VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`;
          
          db.any(queryString, [drugId, drugPrimaryName, globalStatus, countryName, diseaseName, currentStatus, therapeuticArea, companyRole, drugTherapeuticModality])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }

exports.drugMapDrugProgramPanel = drugMapDrugProgramPanel;

