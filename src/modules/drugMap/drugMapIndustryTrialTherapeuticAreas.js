const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = [
  "industryTrialTherapeuticAreas_1",
  "industryTrialTherapeuticAreas_2",
  "industryTrialTherapeuticAreas_3",
  "industryTrialTherapeuticAreas_4",
];

function drugMapIndustryTrialTherapeuticAreas () {
    tables.forEach(async element => {
      await readCsvFile(`../dataCsv/drugMap/${element}.csv`).then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let trialTherapeuticAreas = csvData[i]["trialTherapeuticAreas"];
          let trialDiseases = csvData[i]["trialDiseases"];
          let trialPatientSegments = csvData[i]["trialPatientSegments"];
          
          const queryString = `INSERT INTO "drugMapIndustryTrialTherapeuticAreas" ("trialId", "trialTherapeuticAreas", "trialDiseases", "trialPatientSegments")
          VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialId, trialTherapeuticAreas, trialDiseases, trialPatientSegments])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }); 
}
exports.drugMapIndustryTrialTherapeuticAreas = drugMapIndustryTrialTherapeuticAreas;