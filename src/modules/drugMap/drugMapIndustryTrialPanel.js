const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function drugMapIndustryTrialPanel () {
    readCsvFile("../dataCsv/drugMap/industryTrialPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let recordUrl = csvData[i]["recordUrl"];
          let trialId = csvData[i]["trialId"];
          let trialTitle = csvData[i]["trialTitle"];
          let trialStatus = csvData[i]["trialStatus"];
          let trialPhase = csvData[i]["trialPhase"];
          let trialStartDate = csvData[i]["trialStartDate"];
          let trialPrimaryCompletionDate = csvData[i]["trialPrimaryCompletionDate"];
          let trialTargetAccrual = csvData[i]["trialTargetAccrual"];
          let trialTargetAccrualText = csvData[i]["trialTargetAccrualText"];
          let trialOutcomeDetails = csvData[i]["trialOutcomeDetails"];
          let design = csvData[i]["design"];
          let mechanismOfAction = csvData[i]["MechanismOfAction"];
          let trialName = csvData[i]["trialName"];
          let PBITrialStatus = csvData[i]["PBI_trialStatus"];
          let drugList = csvData[i]["drugList"];
          let designDrugList = csvData[i]["design_drugList"];
          
          
          const queryString = `INSERT INTO "drugMapIndustryTrialPanel" ("recordUrl", "trialId", "trialTitle", "trialStatus", "trialPhase", "trialStartDate", "trialPrimaryCompletionDate",  "trialTargetAccrual",
          "trialTargetAccrualText", "trialOutcomeDetails", "design", "mechanismOfAction", "trialName", "PBITrialStatus", "drugList", "designDrugList")
          VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16)`;
          
          db.any(queryString, [recordUrl, trialId, trialTitle, trialStatus, trialPhase, trialStartDate, trialPrimaryCompletionDate, trialTargetAccrual, trialTargetAccrualText, trialOutcomeDetails,
            design, mechanismOfAction, trialName, PBITrialStatus, drugList, designDrugList])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }

exports.drugMapIndustryTrialPanel = drugMapIndustryTrialPanel;