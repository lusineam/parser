const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsOrganizationGeoLocation () {
    readCsvFile("../dataCsv/clinOps/organizationGeoLocation.csv").then((csvData) => {
        for (let i in csvData) {
        
          let organizationId = csvData[i]["organizationId"];
          let lat = csvData[i]["lat"];
          let lon = csvData[i]["lon"];
          
          const queryString = `INSERT INTO "clinOpsOrganizationGeoLocation" ("organizationId", "lat", "lon") VALUES ($1, $2, $3)`;
          
          db.any(queryString, [organizationId, lat, lon])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsOrganizationGeoLocation = clinOpsOrganizationGeoLocation;