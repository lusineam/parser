const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsIndustryNewTrialPanel () {
    readCsvFile("../dataCsv/clinOps/industryTrialPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let trialPhase = csvData[i]["trialPhase"];
          let trialStatus = csvData[i]["trialStatus"];
          let trialStartDate = csvData[i]["trialStartDate"];
          
          
          if (trialStartDate === "" ) {
            trialStartDate = null;
          }
          

          const queryString = `INSERT INTO "clinOpsIndustryNewTrialPanel" ("trialId", "trialPhase", "trialStatus", "trialStartDate") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialId, trialPhase, trialStatus, trialStartDate])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsIndustryNewTrialPanel = clinOpsIndustryNewTrialPanel;
