const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsIndustryTrialDrugsTested () {
    readCsvFile("../dataCsv/clinOps/industryTrialDrugsTested.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let drugId = csvData[i]["drugId"];
          let drugNameId = csvData[i]["drugNameId"];
          let drugName = csvData[i]["drugName"];
          let role = csvData[i]["role"];
          let primaryDrugList = csvData[i]["primaryDrugList"];
          let drugRole = csvData[i]["drugRole"];
          let drugList = csvData[i]["drugList"];
         
          
          const queryString = `INSERT INTO "clinOpsIndustryTrialDrugsTested" ("trialId", "drugId", "drugNameId", "drugName", "role", "primaryDrugList", "drugRole", "drugList")
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`;
          
          db.any(queryString, [trialId, drugId, drugNameId, drugName, role, primaryDrugList, drugRole, drugList])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsIndustryTrialDrugsTested = clinOpsIndustryTrialDrugsTested;