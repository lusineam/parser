const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsNewLocation () {
    readCsvFile("../dataCsv/clinOps/organizationPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let organizationId = csvData[i]["organizationId"];
          let organizationCountryName = csvData[i]["organizationCountryName"];
          
          const queryString = `INSERT INTO "clinOpsNewLocation" ("organizationId", "organizationCountryName") VALUES ($1, $2)`;
          
          db.any(queryString, [organizationId, organizationCountryName])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsNewLocation = clinOpsNewLocation;