const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsIndustryTrialPrimaryEndpoint () {
    readCsvFile("../dataCsv/clinOps/industryTrialPrimaryEndpoint.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let primaryEndpointGroup = csvData[i]["primaryEndpointGroup"];
          let primaryEndpointSubGroup = csvData[i]["primaryEndpointSubGroup"];
          let primaryEndpoint = csvData[i]["primaryEndpoint"];
          
          const queryString = `INSERT INTO "clinOpsIndustryTrialPrimaryEndpoint" ("trialId", "primaryEndpointGroup", "primaryEndpointSubGroup", "primaryEndpoint") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialId, primaryEndpointGroup, primaryEndpointSubGroup, primaryEndpoint])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsIndustryTrialPrimaryEndpoint = clinOpsIndustryTrialPrimaryEndpoint;