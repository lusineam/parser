const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = [
  "organizationTrialInvestigators_1",
  "organizationTrialInvestigators_2",
  "organizationTrialInvestigators_3"
];

function clinOpsOrganizationTrialInvestigators () {
  tables.forEach(async element => {
    await readCsvFile(`../dataCsv/clinOps/${element}.csv`).then((csvData) => {        
      for (let i in csvData) {

          let organizationId = csvData[i]["organizationId"];
          let trialId = csvData[i]["trialId"];
          let investigatorId = csvData[i]["investigatorId"];
          let affiliation = csvData[i]["affiliation"];
          let trialDiseases = csvData[i]["trialDiseases"];
          let investigatorDisease = csvData[i]["investigator_disease"];
          let trialIdDisease = csvData[i]["trialId_disease"];
          let trialStatus = csvData[i]["trialStatus"];
          let PBITrialStatus = csvData[i]["PBI_trialStatus"];
          let ongoingTrial = csvData[i]["ongoingTrial"];
          let terminatedTrial = csvData[i]["terminatedTrial"];
          let completedTrial = csvData[i]["completedTrial"];
          let plannedTrial = csvData[i]["plannedTrial"];
          
          if (organizationId === "" ) {
            organizationId = null;
          }
          if (trialId === "" ) {
            trialId = null;
          }
          if (investigatorId === "" ) {
            investigatorId = null;
          }
          if (ongoingTrial === "" ) {
            ongoingTrial = null;
          }
          if (terminatedTrial === "" ) {
            terminatedTrial = null;
          }
          if (completedTrial === "" ) {
            completedTrial = null;
          }
          if (plannedTrial === "" ) {
            plannedTrial = null;
          }

          const queryString = `INSERT INTO "clinOpsOrganizationTrialInvestigators" ("organizationId", "trialId", "investigatorId", "affiliation", "trialDiseases", "investigatorDisease",
          "trialIdDisease", "trialStatus", "PBITrialStatus", "ongoingTrial", "terminatedTrial", "completedTrial", "plannedTrial")
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)`;
          
          db.any(queryString, [organizationId, trialId, investigatorId, affiliation, trialDiseases, investigatorDisease, trialIdDisease, trialStatus, PBITrialStatus,
            ongoingTrial, terminatedTrial, completedTrial, plannedTrial])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }); 
 }

exports.clinOpsOrganizationTrialInvestigators = clinOpsOrganizationTrialInvestigators;
