const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsInvestigatorPanel () {
    readCsvFile("../dataCsv/clinOps/investigatorPanel.csv").then((csvData) => {
        for (let i in csvData) {

          let recordUrl = csvData[i]["recordUrl"];
          let investigatorId = csvData[i]["investigatorId"];
          let investigatorNPI = csvData[i]["investigatorNPI"];
          let investigatorFirstName = csvData[i]["investigatorFirstName"];
          let investigatorMiddleInitial = csvData[i]["investigatorMiddleInitial"];
          let investigatorLastName = csvData[i]["investigatorLastName"];
          let investigatorLastTrialStartDate = csvData[i]["investigatorLastTrialStartDate"];
          let updatedDate = csvData[i]["updatedDate"];
          let emailList = csvData[i]["EmailList"];
          let phoneNumberList = csvData[i]["PhoneNumberList"];
          let faxList = csvData[i]["FaxList"];
          let primaryOrganizationId = csvData[i]["primaryOrganizationId"];
          let primaryOrganizationName = csvData[i]["primaryOrganizationName"];
          let primaryOorganizationType = csvData[i]["primaryOorganizationType"];
          let primaryOrganizationUrl = csvData[i]["primaryOrganizationUrl"];
          
          if (investigatorId === "" ) {
            investigatorId = null;
          }
          if (investigatorNPI === "" ) {
            investigatorNPI = null;
          }
          if (investigatorLastTrialStartDate === "" ) {
            investigatorLastTrialStartDate = null;
          }
          if (updatedDate === "" ) {
            updatedDate = null;
          }
          if (primaryOrganizationId === "" ) {
            primaryOrganizationId = null;
          }

          const queryString = `INSERT INTO "clinOpsInvestigatorPanel" ("recordUrl", "investigatorId", "investigatorNPI", "investigatorFirstName", "investigatorMiddleInitial", "investigatorLastName",
          "investigatorLastTrialStartDate", "updatedDate", "emailList", "phoneNumberList", "faxList", "primaryOrganizationId", "primaryOrganizationName", "primaryOorganizationType", "primaryOrganizationUrl")
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)`;
          
          db.any(queryString, [recordUrl, investigatorId, investigatorNPI, investigatorFirstName, investigatorMiddleInitial, investigatorLastName, investigatorLastTrialStartDate, updatedDate, emailList,
            phoneNumberList, faxList, primaryOrganizationId, primaryOrganizationName, primaryOorganizationType, primaryOrganizationUrl])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsInvestigatorPanel = clinOpsInvestigatorPanel;
