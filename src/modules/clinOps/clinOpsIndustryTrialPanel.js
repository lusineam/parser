const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsIndustryTrialPanel () {
    readCsvFile("../dataCsv/clinOps/industryTrialPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let recordUrl = csvData[i]["recordUrl"];
          let trialId = csvData[i]["trialId"];
          let trialTitle = csvData[i]["trialTitle"];
          let trialStatus = csvData[i]["trialStatus"];
          let trialPhase = csvData[i]["trialPhase"];
          let trialStartDate = csvData[i]["trialStartDate"];
          let trialTargetAccrual = csvData[i]["trialTargetAccrual"];
          let trialIdentifiedSites = csvData[i]["trialIdentifiedSites"];
          let trialPrimaryCompletionDate = csvData[i]["trialPrimaryCompletionDate"];
          let trialPrimaryEndpointsReported = csvData[i]["trialPrimaryEndpointsReported"];
          let trialReportedSites = csvData[i]["trialReportedSites"];
          let trialPatientsPerSitePerMonth = csvData[i]["trialPatientsPerSitePerMonth"];
          let totalTrialDuration = csvData[i]["totalTrialDuration"];
          let trialEnrollmentDuration = csvData[i]["trialEnrollmentDuration"];
          let trialEnrollmentDurationPct = csvData[i]["trialEnrollmentDurationPct"];
          
          if (trialStartDate === "" ) {
            trialStartDate = null;
          }
          if (trialTargetAccrual === "" ) {
            trialTargetAccrual = null;
          }
          if (trialIdentifiedSites === "" ) {
            trialIdentifiedSites = null;
          }
          if (trialPrimaryCompletionDate === "" ) {
            trialPrimaryCompletionDate = null;
          }
          if (trialPrimaryEndpointsReported === "" ) {
            trialPrimaryEndpointsReported = null;
          }
          if (trialReportedSites === "" ) {
            trialReportedSites = null;
          }
          if (trialPatientsPerSitePerMonth === "" ) {
            trialPatientsPerSitePerMonth = null;
          }
          if (totalTrialDuration === "" ) {
            totalTrialDuration = null;
          }
          if (trialEnrollmentDuration === "" ) {
            trialEnrollmentDuration = null;
          }
          if (trialEnrollmentDurationPct === "" ) {
            trialEnrollmentDurationPct = null;
          }

          const queryString = `INSERT INTO "clinOpsIndustryTrialPanel" ("recordUrl", "trialId", "trialTitle", "trialStatus", "trialPhase", "trialStartDate", "trialTargetAccrual", "trialIdentifiedSites", 
          "trialPrimaryCompletionDate", "trialPrimaryEndpointsReported", "trialReportedSites", "trialPatientsPerSitePerMonth", "totalTrialDuration", "trialEnrollmentDuration", "trialEnrollmentDurationPct")
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)`;
          
          db.any(queryString, [recordUrl, trialId, trialTitle, trialStatus, trialPhase, trialStartDate, trialTargetAccrual, trialIdentifiedSites, trialPrimaryCompletionDate,
            trialPrimaryEndpointsReported, trialReportedSites, trialPatientsPerSitePerMonth, totalTrialDuration, trialEnrollmentDuration, trialEnrollmentDurationPct])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsIndustryTrialPanel = clinOpsIndustryTrialPanel;
