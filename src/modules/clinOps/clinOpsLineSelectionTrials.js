const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsLineSelectionTrials () {
    readCsvFile("../dataCsv/clinOps/LineSelectionTrials.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let trialDiseases = csvData[i]["trialDiseases"];
          let trialPatientSegments = csvData[i]["trialPatientSegments"];
          let diseaseId = csvData[i]["Disease_Id"];
          
          const queryString = `INSERT INTO "clinOpsLineSelectionTrials" ("trialId", "trialDiseases", "trialPatientSegments", "diseaseId") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialId, trialDiseases, trialPatientSegments, diseaseId])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsLineSelectionTrials = clinOpsLineSelectionTrials;
