const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsOrganizationPanel () {
    readCsvFile("../dataCsv/clinOps/organizationPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let recordUrl = csvData[i]["recordUrl"];
          let organizationId = csvData[i]["organizationId"];
          let organizationName = csvData[i]["organizationName"];
          let organizationType = csvData[i]["organizationType"];
          let organizationCountryName = csvData[i]["organizationCountryName"];
          
          const queryString = `INSERT INTO "clinOpsOrganizationPanel" ("recordUrl", "organizationId", "organizationName", "organizationType", "organizationCountryName")
            VALUES ($1, $2, $3, $4, $5)`;
          
          db.any(queryString, [recordUrl, organizationId, organizationName, organizationType, organizationCountryName])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsOrganizationPanel = clinOpsOrganizationPanel;