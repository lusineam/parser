const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = [
  "investigatorDiseaseTiers_1",
  "investigatorDiseaseTiers_2"
];

function clinOpsInvestigatorDiseaseTiers () {
  tables.forEach(async element => {
    await readCsvFile(`../dataCsv/clinOps/${element}.csv`).then((csvData) => {
        for (let i in csvData) {

          let investigatorId = csvData[i]["investigatorId"];
          let diseaseTier = csvData[i]["diseaseTier"];
          let trialDiseases = csvData[i]["trialDiseases"];
          let investigatorDisease = csvData[i]["investigator_disease"];
          let recruitmentPerformance = csvData[i]["Recruitment_Performance"];

          const queryString = `INSERT INTO "clinOpsInvestigatorDiseaseTiers" ("investigatorId", "diseaseTier", "trialDiseases", "investigatorDisease", "recruitmentPerformance")
            VALUES ($1, $2, $3, $4, $5)`;
          
          db.any(queryString, [investigatorId, diseaseTier, trialDiseases, investigatorDisease, recruitmentPerformance])            
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
      }).catch((error) => {
        console.log(error);
      });
    }); 
   }
      
  exports.clinOpsInvestigatorDiseaseTiers = clinOpsInvestigatorDiseaseTiers;