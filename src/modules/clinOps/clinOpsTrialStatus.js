const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsTrialStatus () {
    readCsvFile("../dataCsv/clinOps/industryTrialPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialStatus = csvData[i]["trialStatus"];
          
          const queryString = `INSERT INTO "clinOpsTrialStatus" ("trialStatus") VALUES ($1)`;
          
          db.any(queryString, [trialStatus])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsTrialStatus = clinOpsTrialStatus;