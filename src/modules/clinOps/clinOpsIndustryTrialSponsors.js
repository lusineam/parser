const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsIndustryTrialSponsors () {
    readCsvFile("../dataCsv/clinOps/industryTrialSponsors.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let name = csvData[i]["name"];
          let parentCompanyName = csvData[i]["parentCompanyName"];
          let sponsorList = csvData[i]["sponsorList"];
          
          const queryString = `INSERT INTO "clinOpsIndustryTrialSponsors" ("trialId", "name", "parentCompanyName", "sponsorList") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialId, name, parentCompanyName, sponsorList])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsIndustryTrialSponsors = clinOpsIndustryTrialSponsors;
