const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsTrialPhase () {
    readCsvFile("../dataCsv/clinOps/industryTrialPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialPhase = csvData[i]["trialPhase"];
          
          const queryString = `INSERT INTO "clinOpsTrialPhase" ("trialPhase") VALUES ($1)`;
          
          db.any(queryString, [trialPhase])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsTrialPhase = clinOpsTrialPhase;