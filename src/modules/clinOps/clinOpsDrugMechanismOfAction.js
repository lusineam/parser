const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsDrugMechanismOfAction () {
    readCsvFile("../dataCsv/clinOps/drugMechanismOfAction.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let HRCHY1 = csvData[i]["HRCHY_1"];
          let HRCHY2 = csvData[i]["HRCHY_2"];
          let HRCHY3 = csvData[i]["HRCHY_3"];
          let HRCHY4 = csvData[i]["HRCHY_4"];
          let HRCHY5 = csvData[i]["HRCHY_5"];
          let HRCHY6 = csvData[i]["HRCHY_6"];
         
          
          const queryString = `INSERT INTO "clinOpsDrugMechanismOfAction" ("drugId", "HRCHY1", "HRCHY2", "HRCHY3", "HRCHY4", "HRCHY5", "HRCHY6")
            VALUES ($1, $2, $3, $4, $5, $6, $7)`;
          
          db.any(queryString, [drugId, HRCHY1, HRCHY2, HRCHY3, HRCHY4, HRCHY5, HRCHY6])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsDrugMechanismOfAction = clinOpsDrugMechanismOfAction;
