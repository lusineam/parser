const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function clinOpsIndustryTrialCountries () {
    readCsvFile("../dataCsv/clinOps/industryTrialCountries.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let trialCountries = csvData[i]["trialCountries"];
          let trialCountryNumber = csvData[i]["trialCountryNumber"];
        
          
          const queryString = `INSERT INTO "clinOpsIndustryTrialCountries" ("trialId", "trialCountries", "trialCountryNumber") VALUES ($1, $2, $3)`;
          
          db.any(queryString, [trialId, trialCountries, trialCountryNumber])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.clinOpsIndustryTrialCountries = clinOpsIndustryTrialCountries;
