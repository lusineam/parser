const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = [
  "industryTrialTherapeuticAreas_1",
  "industryTrialTherapeuticAreas_2",
  "industryTrialTherapeuticAreas_3",
  "industryTrialTherapeuticAreas_4",
];

function clinOpsIndustryTrialTherapeuticAreas() {
  tables.forEach(async (element) => {
    await readCsvFile(`../dataCsv/clinOps/${element}.csv`)
      .then((csvData) => {
        for (let i in csvData) {
          let trialId = csvData[i]["trialId"];
          let trialTherapeuticAreas = csvData[i]["trialTherapeuticAreas"];
          let trialDiseases = csvData[i]["trialDiseases"];
          let trialPatientSegments = csvData[i]["trialPatientSegments"];
          let diseaseList = csvData[i]["diseaseList"];
          let TAList = csvData[i]["TAList"];
          let ptSegmentList = csvData[i]["ptSegmentList"];
          let diseaseId = csvData[i]["Disease_Id"];
          let trialMeshTerms = csvData[i]["trialMeshTerms"];
          let trialIdDisease = csvData[i]["trialId_disease"];

          const queryString = `INSERT INTO "clinOpsIndustryTrialTherapeuticAreas" ("trialId", "trialTherapeuticAreas", "trialDiseases", "trialPatientSegments", "diseaseList", "TAList", "ptSegmentList", "diseaseId", "trialMeshTerms", "trialIdDisease")
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`;

          db.any(queryString, [
            trialId,
            trialTherapeuticAreas,
            trialDiseases,
            trialPatientSegments,
            diseaseList,
            TAList,
            ptSegmentList,
            diseaseId,
            trialMeshTerms,
            trialIdDisease,
          ])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  });
}

exports.clinOpsIndustryTrialTherapeuticAreas =
  clinOpsIndustryTrialTherapeuticAreas;
