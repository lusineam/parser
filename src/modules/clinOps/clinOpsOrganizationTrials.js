const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = [
  "organizationTrialInvestigators_1",
  "organizationTrialInvestigators_2",
  "organizationTrialInvestigators_3"
];

function clinOpsOrganizationTrials () {
  tables.forEach(async element => {
    await readCsvFile(`../dataCsv/clinOps/${element}.csv`).then((csvData) => {        
      for (let i in csvData) {
        
          let organizationId = csvData[i]["organizationId"];
          let trialId = csvData[i]["trialId"];
          
          const queryString = `INSERT INTO "clinOpsOrganizationTrials" ("organizationId", "trialId") VALUES ($1, $2)`;
          
          db.any(queryString, [organizationId, trialId])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }); 
 }
      
  exports.clinOpsOrganizationTrials = clinOpsOrganizationTrials;