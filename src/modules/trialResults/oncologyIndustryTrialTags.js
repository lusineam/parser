const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialTags () {
  readCsvFile(`../dataCsv/trialResults/industryTrialTags.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let trialId = csvData[i]["trialId"];
      let trialTags = csvData[i]["trialTags"];
      
      const queryString = `INSERT INTO "oncologyIndustryTrialTags" ("id", "trialId", "trialTags") VALUES ($1, $2, $3)`;

      db.any(queryString, [id, trialId, trialTags])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyIndustryTrialTags = oncologyIndustryTrialTags;
