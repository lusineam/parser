const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyNSCLCPivotalResults () {
  readCsvFile(`../dataCsv/trialResults/trial_Results.csv`).then((csvData) => {

    for (let i in csvData) {
      let id = Number(i) + 1;
      let trialId = csvData[i]["Trial_ID"];
      let resultsLevel = csvData[i]["Results_Level"];
      let uniqueArmId = csvData[i]["Unique_Arm_ID"];
      let subgroupId = csvData[i]["Subgroup_ID"];
      let eventId = csvData[i]["Event_ID"];
      let eventDate = csvData[i]["Event_Date"];
      let eventCongressDescription = csvData[i]["Event_Congress_Description"];
      let eventDataCutoffTime = csvData[i]["Event_Data_Cutoff_Time"];
      let numberOfPatients = csvData[i]["Number_of_Patients"];
      let url = csvData[i]["url"];
      let endpoint = csvData[i]["Endpoint"];
      let endpointCategory = csvData[i]["Endpoint_Category"];
      let value = csvData[i]["Value"];
      let valuePeriod = csvData[i]["Value_Period(mos)"];
      let valueHRCI = csvData[i]["Value_HR_CI"];
      let valueHRCompareToArmID = csvData[i]["Value_HR_CompareToArmID"];
      let endpointType = csvData[i]["Endpoint_Type"];
      let ORRRECIST = csvData[i]["ORR_RECIST"];
      let linkId = csvData[i]["Link_ID"];
      let HRAssessedMethod = csvData[i]["HR_Assessed_Method"];
      let assessment = csvData[i]["Assessment"];
      let waterfallId = csvData[i]["Waterfall_ID"];
      let trialIdLevel = csvData[i]["Trial_ID_Level"];
      let notes = csvData[i]["Notes"];
      let unit = csvData[i]["Unit"];
      
      if (eventId === "" ) {
        eventId = null;
      }
      if (eventDate === "" ) {
        eventDate = null;
      }
      if (numberOfPatients === "" ) {
        numberOfPatients = null;
      }
      if (value === "" ) {
        value = null;
      }

      const queryString = `INSERT INTO "oncologyNSCLCPivotalResults" ("id", "trialId", "resultsLevel", "uniqueArmId", "subgroupId", "eventId", "eventDate", "eventCongressDescription", "eventDataCutoffTime", 
      "numberOfPatients","url", "endpoint", "endpointCategory", "value", "valuePeriod", "valueHRCI", "valueHRCompareToArmID", "endpointType", "ORRRECIST", "linkId", "HRAssessedMethod", "assessment", 
        "waterfallId", "trialIdLevel", "notes", "unit") 
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26)`;
      db.any(queryString, [id, trialId, resultsLevel, uniqueArmId, subgroupId, eventId, eventDate, eventCongressDescription, eventDataCutoffTime, numberOfPatients, url, endpoint,
        endpointCategory, value, valuePeriod, valueHRCI, valueHRCompareToArmID, endpointType, ORRRECIST, linkId, HRAssessedMethod, assessment, waterfallId, trialIdLevel, notes, unit
      ])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyNSCLCPivotalResults  = oncologyNSCLCPivotalResults;
