const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyNSCLCPivotalPatients () {
  readCsvFile(`../dataCsv/trialResults/trial_Patients.csv`).then((csvData) => {
    for (let i in csvData) {

      let trialNCT = csvData[i]["Trial_NCT"];
      let trialName = csvData[i]["Trial_Name"];
      let source = csvData[i]["Source"];
      let indication = csvData[i]["Indication"];
      let trialId = csvData[i]["Trial_ID"];
      let designRandomized = csvData[i]["Design_Randomized"];
      let estimatedSize = csvData[i]["Estimated_Size"];
      let actualSize = csvData[i]["Actual_Size"];
      let patientsAgesMin = csvData[i]["Patients_Ages_Min"];
      let patientsAgesMax = csvData[i]["Patients_Ages_Max"];
      let asianPatientsNumber = csvData[i]["Asian_Patients_Number"];
      let chinesePatientsNumber = csvData[i]["Chinese_Patients_Number"];
      let otherPrimaryEndpoint = csvData[i]["Other_Primary_Endpoint"];
      let otherSecondaryEndpoint = csvData[i]["Other_Secondary_Endpoint"];
      let primaryEndpoint = csvData[i]["design_DrugList"];
      let secondaryEndpoint = csvData[i]["Secondary_Endpoint"];
      let trialPhase = csvData[i]["Trial_Phase"];
      let uniqueArmId = csvData[i]["Unique_Arm_ID"];
      let drugOrder = csvData[i]["Drug_Order"];
      let drugName = csvData[i]["Drug_Name"];
      let drugId = csvData[i]["Drug_ID"];
      let regimen = csvData[i]["Regimen"];
      let dosingAmount = csvData[i]["Dosing_Amount"];
      let dosingUnit = csvData[i]["Dosing_Unit"];
      let dosingRoute = csvData[i]["Dosing_Route"];
      let dosingFrequency = csvData[i]["Dosing_Frequency"];
      let dosingCycleLength = csvData[i]["Dosing_Cycle_Length"];
      let dosingDurationWithinACycle = csvData[i]["Dosing_Duration_Within_A_Cycle"];
      let dosingDuration = csvData[i]["Dosing_Duration"];
      let arm = csvData[i]["Arm"];
      let interveneType = csvData[i]["Intervene_Type"];
      let mechanismOfAction = csvData[i]["mechanism_of_action"];
      let moaList = csvData[i]["MoA_List"];
      let armName = csvData[i]["Arm_Name"];
      let resultsLevel = csvData[i]["Results_Level"];
      let subGroupName = csvData[i]["Subgroup_Name"];
      let subGroupId = csvData[i]["Subgroup_ID"];
      let linkId = csvData[i]["Link_ID"];
      let drugNameClean = csvData[i]["Drug_Name_Clean"];
      let eventId = csvData[i]["Event_ID"];
      let waterfallId = csvData[i]["Waterfall_ID"];
      let KRAS = csvData[i]["KRAS"];
      let otherSubtype = csvData[i]["Other subtype"];
      let maintenanceConsolidation = csvData[i]["Maintenance/Consolidation"];
      let STK11 = csvData[i]["STK11"];
      let NRAS = csvData[i]["NRAS"];
      let rxEligibility = csvData[i]["Rx_eligibility"];
      let TMB = csvData[i]["TMB"];
      let MSI = csvData[i]["MSI"];
      let NTRK = csvData[i]["NTRK"];
      let RET = csvData[i]["RET"];
      let BRAF = csvData[i]["BRAF"];
      let HER2 = csvData[i]["HER2"];
      let PDL1Level = csvData[i]["PD-L1_Level"];
      let priorRx = csvData[i]["Prior_Rx"];
      let histology = csvData[i]["Histologic Grade"];
      let ALK = csvData[i]["ALK"];
      let EGFR = csvData[i]["EGFR"];
      let EGFRExon20 = csvData[i]["EGFR_exon_20"];
      let ROS1 = csvData[i]["ROS1"];
      let cMET = csvData[i]["c-MET"];
      let Intracranial = csvData[i]["Intracranial"];
      let pivotalFLag = csvData[i]["pivotalFLag"];
      let BRCA = csvData[i]["BRCA"];
      let HRAS = csvData[i]["HRAS"];
      let FGFR = csvData[i]["FGFR"];
      let trialCitelineUrl = csvData[i]["Trial_Citeline_url"];
      let drugUrl = csvData[i]["Drug_url"];
      let patientFeature = csvData[i]["Patient_Feature"];
      let EBVStatus = csvData[i]["EBV Status"];
      let histologicGrade = csvData[i]["Histologic Grade"];
      let primaryLocation = csvData[i]["Primary_Location"];
      let MSIOrMMRstatus = csvData[i]["MSI or MMR status"];
      let drugNotes = csvData[i]["Drug_Notes"];
      let OldID = csvData[i]["_OldID"];
      let primarySite = csvData[i]["Primary_Site"];

      if (trialId === "" ) {
        trialId = null;
      }
      if (actualSize === "" ) {
        actualSize = null;
      }
      if (patientsAgesMin === "" ) {
        patientsAgesMin = null;
      }
      if (patientsAgesMax === "" ) {
        patientsAgesMax = null;
      }
      if (asianPatientsNumber === "" ) {
        asianPatientsNumber = null;
      }
      if (chinesePatientsNumber === "" ) {
        chinesePatientsNumber = null;
      }
      if (drugOrder === "" ) {
        drugOrder = null;
      }
      if (drugId === "" ) {
        drugId = null;
      }
      if (eventId === "" ) {
        eventId = null;
      }
      
      const queryString = `INSERT INTO "oncologyNSCLCPivotalPatients" ("trialNCT", "trialName", "source", "indication", "trialId", "designRandomized", "estimatedSize", "actualSize", "patientsAgesMin",
        "patientsAgesMax", "asianPatientsNumber", "chinesePatientsNumber", "otherPrimaryEndpoint", "otherSecondaryEndpoint",  "primaryEndpoint", "secondaryEndpoint", "trialPhase", 
        "uniqueArmId", "drugOrder", "drugName", "drugId", "regimen", "dosingAmount", "dosingUnit", "dosingRoute", "dosingFrequency", "dosingCycleLength", "dosingDurationWithinACycle", "dosingDuration",
        "arm", "interveneType", "mechanismOfAction", "moaList", "armName", "resultsLevel", "subGroupName", "subGroupId", "linkId", "drugNameClean", "eventId", "waterfallId", "KRAS", "otherSubtype",
        "maintenanceConsolidation", "STK11", "NRAS", "rxEligibility", "TMB", "NTRK", "RET", "BRAF", "HER2", "PDL1Level", "priorRx", "histology", "ALK", "EGFR", "Intracranial", "pivotalFLag", "BRCA",
         "HRAS", "FGFR", "trialCitelineUrl", "drugUrl", "patientFeature", "EBVStatus", "histologicGrade", "primaryLocation", "MSIOrMMRstatus", "MSI", "EGFRExon20", "ROS1", "cMET", "drugNotes", "OldID", "primarySite") 
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30, $31, $32, $33, $34, $35, $36, $37, $38, $39, $40,
          $41, $42, $43, $44, $45, $46, $47, $48, $49, $50, $51, $52, $53, $54, $55, $56, $57, $58, $59, $60, $61, $62, $63, $64, $65, $66, $67, $68, $69, $70, $71, $72, $73, $74, $75, $76)`;

      db.any(queryString, [trialNCT, trialName, source, indication, trialId, designRandomized, estimatedSize, actualSize, patientsAgesMin, patientsAgesMax, asianPatientsNumber,
        chinesePatientsNumber, otherPrimaryEndpoint, otherSecondaryEndpoint, primaryEndpoint, secondaryEndpoint, trialPhase, uniqueArmId, drugOrder, drugName, drugId, regimen, 
        dosingAmount, dosingUnit, dosingRoute, dosingFrequency, dosingCycleLength, dosingDurationWithinACycle, dosingDuration, arm, interveneType, mechanismOfAction, moaList, armName, resultsLevel,
        subGroupName, subGroupId, linkId, drugNameClean, eventId, waterfallId, KRAS, otherSubtype, maintenanceConsolidation, STK11, NRAS, rxEligibility, TMB, NTRK,  RET, BRAF, HER2, PDL1Level,
        priorRx, histology, ALK, EGFR, Intracranial, pivotalFLag, BRCA, HRAS, FGFR, trialCitelineUrl, drugUrl, patientFeature, EBVStatus, histologicGrade, primaryLocation, MSIOrMMRstatus, MSI,
        EGFRExon20, ROS1, cMET, drugNotes, OldID, primarySite])
        .then((data) => {})
        .catch((error) => {
          console.log(error, data);
        });
    }
  });
}

exports.oncologyNSCLCPivotalPatients  = oncologyNSCLCPivotalPatients;