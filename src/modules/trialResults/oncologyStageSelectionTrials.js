const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyStageSelectionTrials () {
  readCsvFile(`../dataCsv/trialResults/stageSelectionTrials.csv`).then((csvData) => {
    for (let i in csvData) {

      let trialPatientSegments = csvData[i]["trialPatientSegments"];
      let trialId = csvData[i]["trialId"];
      let trialDiseases = csvData[i]["trialDiseases"];
      let diseaseId = csvData[i]["Disease_Id"];

      if (trialId === "" ) {
        trialId = null;
      }
      const queryString = `INSERT INTO "oncologyStageSelectionTrials" ("trialPatientSegments", "trialId", "trialDiseases", "diseaseId") VALUES ($1, $2, $3, $4)`;

      db.any(queryString, [trialPatientSegments, trialId, trialDiseases, diseaseId])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyStageSelectionTrials  = oncologyStageSelectionTrials;
