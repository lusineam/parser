const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyNSCLCPivotalEventMilestones () {
  readCsvFile(`../dataCsv/trialResults/trial_Event_Milestones.csv`).then((csvData) => {
    for (let i in csvData) {

      let trialId = csvData[i]["Trial_ID"];
      let eventDate1 = csvData[i]["Event_date 1"];
      let eventDate2 = csvData[i]["Event_date 2"];
      let eventDate3 = csvData[i]["Event_date 3"];
      let eventDate4 = csvData[i]["Event_date 4"];
      let eventDate5 = csvData[i]["Event_date 5"];
      let eventDate6 = csvData[i]["Event_date 6"];
      let eventDate7 = csvData[i]["Event_date 7"];
      let eventDate8 = csvData[i]["Event_date 8"];
      let eventDate9 = csvData[i]["Event_date 9"];
      let eventDate10 = csvData[i]["Event_date 10"];
      let eventDate11 = csvData[i]["Event_date 11"];
      let eventDate12 = csvData[i]["Event_date 12"];
      let resultsLevel = csvData[i]["Results_Level"];
      let trialIdLevel = csvData[i]["Trial_ID_Level"];

      if (eventDate1 === "" ) {
        eventDate1 = null;
      }
      if (eventDate2 === "" ) {
        eventDate2 = null;
      }
      if (eventDate3 === "" ) {
        eventDate3 = null;
      }
      if (eventDate4 === "" ) {
        eventDate4 = null;
      }
      if (eventDate5 === "" ) {
        eventDate5 = null;
      }
      if (eventDate6 === "" ) {
        eventDate6 = null;
      }
      if (eventDate7 === "" ) {
        eventDate7 = null;
      }
      if (eventDate8 === "" ) {
        eventDate8 = null;
      }
      if (eventDate9 === "" ) {
        eventDate9 = null;
      }
      if (eventDate10 === "" ) {
        eventDate10 = null;
      }
      if (eventDate11 === "" ) {
        eventDate11 = null;
      }
      if (eventDate12 === "" ) {
        eventDate12 = null;
      }
      
      const queryString = `INSERT INTO "oncologyNSCLCPivotalEventMilestones" ("trialId", "eventDate1", "eventDate2", "eventDate3", "eventDate4", "eventDate5", "eventDate6", "eventDate7", "eventDate8",
      "eventDate9", "eventDate10", "eventDate11", "eventDate12", "resultsLevel", "trialIdLevel") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)`;

      db.any(queryString, [trialId, eventDate1, eventDate2, eventDate3, eventDate4, eventDate5, eventDate6, eventDate7, eventDate8, eventDate9, eventDate10, eventDate11, eventDate12, resultsLevel, trialIdLevel])
        .then((data) => {})
        .catch((error) => {
          console.log(error, data);
        });
    }
  });
}

exports.oncologyNSCLCPivotalEventMilestones  = oncologyNSCLCPivotalEventMilestones;