const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyNSCLCPivotalResultsScatter () {
  readCsvFile(`../dataCsv/trialResults/trial_Results_Scatter.csv`).then((csvData) => {
    for (let i in csvData) {
      
      let endpointX = csvData[i]["Endpoint_X"];
      let endpointXCategory = csvData[i]["Endpoint_X_Category"];
      let endpointXValue = csvData[i]["Endpoint_X_Value"];
      let endpointY = csvData[i]["Endpoint_Y"];
      let endpointYCategory = csvData[i]["Endpoint_Y_Category"];
      let endpointYValue = csvData[i]["Endpoint_Y_Value"];
      let trialId = csvData[i]["Trial_ID"];
      let uniqueArmId = csvData[i]["Unique_Arm_ID"];
      let subGroupId = csvData[i]["Subgroup_ID"];
      let linkId = csvData[i]["Link_ID"];
      let eventId = csvData[i]["Event_ID"];
      let assessment = csvData[i]["Assessment"];
      let HRAssessedMethod = csvData[i]["HR_Assessed_Method"];
      let valuePeriod = csvData[i]["Value_Period"];
      let waterfallId = csvData[i]["Waterfall_ID"];
      let resultsLevel = csvData[i]["Results_Level"];
      let eventDate = csvData[i]["Event_Date"];
      let eventDataCutoffTime = csvData[i]["Event_Data_Cutoff_Time"];
      let notes = csvData[i]["Notes"];
    
      let today = eventDataCutoffTime;

      if(today !== "") {
      
        let dateValues = today.split("/");
        if(!dateValues[0]) {
        today = dateValues[1] + '-' + "00" + '-' + dateValues[2];
        }
        if(!dateValues[1]) {
          today = "00" + '-' + dateValues[0] + '-' + dateValues[2];
          }

        if(!dateValues[2]) {
          today = dateValues[1] + '-' + dateValues[0] + '-' + "0000";
          }
        
        today = dateValues[1] + '-' + dateValues[0] + '-' + dateValues[2];
        today = today.replace('undefined-', '').replace('-undefined', '');
      }

      if (endpointXValue === "" ) {
        endpointXValue = null;
      }
      if (endpointYValue === "" ) {
        endpointYValue = null;
      }
      if (eventId === "" ) {
        eventId = null;
      }
      if (eventDate === "" ) {
        eventDate = null;
      }
      if (today === "" ) {
        today = null;
      }
      const queryString = `INSERT INTO "oncologyNSCLCPivotalResultsScatter" ("endpointX", "endpointXCategory", "endpointXValue", "endpointY", "endpointYCategory", "endpointYValue", "trialId", "uniqueArmId", 
      "subGroupId","linkId", "eventId", "assessment", "HRAssessedMethod", "valuePeriod", "waterfallId", "resultsLevel", "eventDate", "eventDataCutoffTime", "notes") 
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19)`;
      db.any(queryString, [endpointX, endpointXCategory, endpointXValue, endpointY, endpointYCategory, endpointYValue, trialId, uniqueArmId, subGroupId, linkId, eventId, assessment, HRAssessedMethod,
        valuePeriod, waterfallId, resultsLevel, eventDate, today, notes])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyNSCLCPivotalResultsScatter  = oncologyNSCLCPivotalResultsScatter;