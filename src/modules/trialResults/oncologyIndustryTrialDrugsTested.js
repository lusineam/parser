const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialDrugsTested () {
  readCsvFile(`../dataCsv/trialResults/industryTrialDrugsTested.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let trialId = csvData[i]["trialId"];
      let drugId = csvData[i]["drugId"];
      let drugNameId = csvData[i]["drugNameId"];
      let drugName = csvData[i]["drugName"];
      let drugRole = csvData[i]["drugRole"];
      let drugList = csvData[i]["drugList"];

      const queryString = `INSERT INTO "oncologyIndustryTrialDrugsTested" ("id", "trialId", "drugId", "drugNameId", "drugName", "drugRole", "drugList") VALUES ($1, $2, $3, $4, $5, $6, $7)`;

      db.any(queryString, [id, trialId, drugId, drugNameId, drugName, drugRole, drugList])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyIndustryTrialDrugsTested  = oncologyIndustryTrialDrugsTested;
