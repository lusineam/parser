const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialPrimaryEndpoint () {
  readCsvFile(`../dataCsv/trialResults/industryTrialPrimaryEndpoint.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let primaryEndpointGroup = csvData[i]["primaryEndpointGroup"];
      let primaryEndpointSubGroup = csvData[i]["primaryEndpointSubGroup"];
      let primaryEndpoint = csvData[i]["primaryEndpoint"];
      let trialId = csvData[i]["trialId"];     

      const queryString = `INSERT INTO "oncologyIndustryTrialPrimaryEndpoint" ("id", "primaryEndpointGroup", "primaryEndpointSubGroup", "primaryEndpoint", "trialId") VALUES ($1, $2, $3, $4, $5)`;
      db.any(queryString, [id, primaryEndpointGroup, primaryEndpointSubGroup, primaryEndpoint, trialId])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyIndustryTrialPrimaryEndpoint  = oncologyIndustryTrialPrimaryEndpoint;
