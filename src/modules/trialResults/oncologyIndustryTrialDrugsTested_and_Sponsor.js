const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialDrugsTested_and_Sponsor () {
  readCsvFile(`../dataCsv/trialResults/industryTrialDrugsTested_and_Sponsor.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let trialId = csvData[i]["trialId"];
      let sponsorList = csvData[i]["sponsorList"];
      let drugList = csvData[i]["drugList"];
      let sponsor_trialname = csvData[i]["sponsor_trialname"];

      const queryString = `INSERT INTO "oncologyIndustryTrialDrugsTestedAndSponsor" ("id", "trialId", "sponsorList", "drugList", "sponsorTrialName") VALUES ($1, $2, $3, $4, $5)`;

      db.any(queryString, [id, trialId, sponsorList, drugList, sponsor_trialname])
        .then((data) => {})
        .catch((error) => {
          console.log(error, data);
        });
    }
  });
}

exports.oncologyIndustryTrialDrugsTested_and_Sponsor = oncologyIndustryTrialDrugsTested_and_Sponsor;
