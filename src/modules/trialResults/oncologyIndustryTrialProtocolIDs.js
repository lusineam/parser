const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialProtocolIDs () {
  readCsvFile(`../dataCsv/trialResults/industryTrialProtocolIDs.csv`).then((csvData) => {
    for (let i in csvData) {

      let trialId = csvData[i]["trialId"];   
      let trialProtocolIDs = csvData[i]["trialProtocolIDs"];   
      let trialIdText = csvData[i]["trialIdText"];       

      if (trialIdText === "" ) {
        trialIdText = null;
      }

      const queryString = `INSERT INTO "oncologyIndustryTrialProtocolIDs" ("trialId", "trialProtocolIDs", "trialIdText") VALUES ($1, $2, $3)`;
      db.any(queryString, [trialId, trialProtocolIDs, trialIdText])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyIndustryTrialProtocolIDs  = oncologyIndustryTrialProtocolIDs;
