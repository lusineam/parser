const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialTherapeuticAreas () {
  readCsvFile(`../dataCsv/trialResults/industryTrialTherapeuticAreas.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let trialId = csvData[i]["trialId"];
      let trialTherapeuticAreas = csvData[i]["trialTherapeuticAreas"];
      let trialDiseases = csvData[i]["trialDiseases"];
      let trialPatientSegments = csvData[i]["trialPatientSegments"];
      let diseaseId = csvData[i]["disease_Id"];
      let diseaseList = csvData[i]["diseaseList"];

      const queryString = `INSERT INTO "oncologyIndustryTrialTherapeuticAreas" ("id", "trialId", "trialTherapeuticAreas", "trialDiseases", "trialPatientSegments", "diseaseId", "diseaseList") VALUES ($1, $2, $3, $4, $5, $6, $7)`;

      db.any(queryString, [id, trialId, trialTherapeuticAreas, trialDiseases, trialPatientSegments, diseaseId, diseaseList])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyIndustryTrialTherapeuticAreas  = oncologyIndustryTrialTherapeuticAreas;