const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialCountries () {
  readCsvFile(`../dataCsv/trialResults/industryTrialCountries.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let trialId = csvData[i]["trialId"];
      let trialCountries = csvData[i]["trialCountries"];
      let countryCode = csvData[i]["countryCode"];

      const queryString = `INSERT INTO "oncologyIndustryTrialCountries" ("id", "trialId", "trialCountries", "countryCode") VALUES ($1, $2, $3, $4)`;

      db.any(queryString, [id, trialId, trialCountries, countryCode])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyIndustryTrialCountries = oncologyIndustryTrialCountries;
