const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyIndustryTrialSponsors () {
  readCsvFile(`../dataCsv/trialResults/industryTrialSponsors.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let trialId = csvData[i]["trialId"];
      let name = csvData[i]["name"];
      let type = csvData[i]["type"];
      let parentCompanyName = csvData[i]["parentCompanyName"];
      let parentCompanyHQCountry = csvData[i]["parentCompanyHQCountry"];
      let parentCompanyHQState = csvData[i]["parentCompanyHQState"];
      let sponsorList = csvData[i]["sponsorList"];

      const queryString = `INSERT INTO "oncologyIndustryTrialSponsors" ("id", "trialId", "name", "type", "parentCompanyName", "parentCompanyHQCountry", "parentCompanyHQState", "sponsorList") VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`;

      db.any(queryString, [id, trialId, name, type, parentCompanyName, parentCompanyHQCountry, parentCompanyHQState, sponsorList])
        .then((data) => {})
        .catch((error) => {
          console.log(error, data);
        });
    }
  });
}

exports.oncologyIndustryTrialSponsors = oncologyIndustryTrialSponsors;
