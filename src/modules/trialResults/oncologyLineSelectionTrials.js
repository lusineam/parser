const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function oncologyLineSelectionTrials () {
  readCsvFile(`../dataCsv/trialResults/lineSelectionTrials.csv`).then((csvData) => {
    for (let i in csvData) {

      let id = Number(i) + 1;
      let trialPatientSegments = csvData[i]["trialPatientSegments"];
      let trialId = csvData[i]["trialId"];
      let trialDiseases = csvData[i]["trialDiseases"];
      let diseaseId = csvData[i]["Disease_Id"];

      const queryString = `INSERT INTO "oncologyLineSelectionTrials" ("id", "trialPatientSegments", "trialId", "trialDiseases", "diseaseId") VALUES ($1, $2, $3, $4, $5)`;

      db.any(queryString, [id, trialPatientSegments, trialId, trialDiseases, diseaseId])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.oncologyLineSelectionTrials  = oncologyLineSelectionTrials;
