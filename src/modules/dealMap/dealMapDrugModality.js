const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function dealMapDrugModality () {
    readCsvFile("../dataCsv/dealMap/drugModality.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let dealId = csvData[i]["dealId"];
          let drugTherapeuticModality = csvData[i]["drugTherapeuticModality"];
          
          if (drugId === "" ) {
            drugId = null;
          }
          if (dealId === "" ) {
            dealId = null;
          }
  
          const queryString = `INSERT INTO "dealMapDrugModality" ("drugId", "dealId", "drugTherapeuticModality") VALUES ($1, $2, $3)`;
          
          db.any(queryString, [drugId, dealId, drugTherapeuticModality])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.dealMapDrugModality = dealMapDrugModality;