const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function dealMapDealProduct () {
    readCsvFile("../dataCsv/dealMap/dealProduct.csv").then((csvData) => {
        for (let i in csvData) {

          let dealId = csvData[i]["dealId"];
          let drugId = csvData[i]["drugId"];
          let brandName = csvData[i]["brandName"];
          let latestPhaseAtTimeOfDeal = csvData[i]["latestPhaseAtTimeOfDeal"];
          let drugNameSynonyms = csvData[i]["drugNameSynonyms"];
          let indicationGroupId = csvData[i]["indicationGroupId"];
          let indicationGroupName = csvData[i]["indicationGroupName"];
          let licenser = csvData[i]["licenser"];
          let licensee = csvData[i]["licensee"];
          let manufacturer = csvData[i]["manufacturer"];
          let clinicalDeveloper = csvData[i]["clinicalDeveloper"];
          let territoriesExcluded = csvData[i]["territoriesExcluded"];
          let territoriesCompany = csvData[i]["territoriesCompany"];
          let territoriesExclusivity = csvData[i]["territoriesExclusivity"];
          let territoriesContinent = csvData[i]["territoriesContinent"];
          let indicationId = csvData[i]["indicationId"];
          let indicationName = csvData[i]["indicationName"];
          let meshId = csvData[i]["meshId"];
          let meshName = csvData[i]["meshName"];

          if (dealId === "" ) {
            dealId = null
          }
          if (drugId === "" ) {
            drugId = null
          }
          if (indicationId === "" ) {
            indicationId = null
          }
          if (meshId === "" ) {
            meshId = null
          }
          if (indicationGroupId === "" ) {
            indicationGroupId = null
          }
          
          const queryString = `INSERT INTO "dealMapDealProduct" ("dealId", "drugId", "brandName", "latestPhaseAtTimeOfDeal", "drugNameSynonyms", "indicationGroupId", "indicationGroupName", "licenser", "licensee", "manufacturer", 
          "clinicalDeveloper", "territoriesExcluded", "territoriesCompany", "territoriesExclusivity", "territoriesContinent", "indicationId", "indicationName", "meshId", "meshName")
              VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19)`;
          
          db.any(queryString, [dealId, drugId, brandName, latestPhaseAtTimeOfDeal, drugNameSynonyms, indicationGroupId, indicationGroupName, licenser, licensee, manufacturer, clinicalDeveloper, territoriesExcluded, territoriesCompany, territoriesExclusivity, 
            territoriesContinent, indicationId, indicationName, meshId, meshName])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
exports.dealMapDealProduct = dealMapDealProduct;
