const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function dealMapDealPanel () {
    readCsvFile("../dataCsv/dealMap/dealPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let recordUrl = csvData[i]["recordUrl"];
          let dealId = csvData[i]["dealId"];
          let dealDate = csvData[i]["dealDate"];
          let dealNumber = csvData[i]["dealNumber"];
          let dealHeadline = csvData[i]["dealHeadline"];
          let dealSummary = csvData[i]["dealSummary"];
          let dealDetails = csvData[i]["dealDetails"];
          let dealStatus = csvData[i]["dealStatus"];
          let totalDealValueUsdM = csvData[i]["totalDealValueUsdM"];
          let updatedDate = csvData[i]["updatedDate"];
          let dealContractLength = csvData[i]["dealContractLength"];
          let totalDealValue = csvData[i]["totalDealValue"];

          if (dealId === "" ) {
            dealId = null;
          }
          if (dealDate === "" ) {
            dealDate = null;
          }
          if (dealNumber === "" ) {
            dealNumber = null;
          }
          if (updatedDate === "" ) {
            updatedDate = null;
          }
          if (dealContractLength === "" ) {
            dealContractLength = null;
          }
          if (totalDealValue === "" ) {
            totalDealValue = null;
          }
  
          const queryString = `INSERT INTO "dealMapDealPanel" ("recordUrl", "dealId", "dealDate", "dealNumber", "dealHeadline", "dealSummary",  "dealDetails", "dealStatus", "totalDealValueUsdM", "updatedDate", "dealContractLength",
          "totalDealValue") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`;
          
          db.any(queryString, [recordUrl, dealId, dealDate, dealNumber, dealHeadline, dealSummary, dealDetails, dealStatus, totalDealValueUsdM, updatedDate, dealContractLength, totalDealValue])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.dealMapDealPanel = dealMapDealPanel;