const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function dealMapDrugTargets () {
    readCsvFile("../dataCsv/dealMap/drugTargets.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let targetName = csvData[i]["targetName"];
          let entrezGeneId = csvData[i]["entrezGeneId"];
          let targetFamilies = csvData[i]["targetFamilies"];
          let targetECNumbers = csvData[i]["targetECNumbers"];
          let targetSynonyms = csvData[i]["targetSynonyms"];
          let dealId = csvData[i]["dealId"];

          if (drugId === "" ) {
            drugId = null;
          }
          if (entrezGeneId === "" ) {
            entrezGeneId = null;
          }
          if (dealId === "" ) {
            dealId = null;
          }
  
          const queryString = `INSERT INTO "dealMapDrugTargets" ("drugId", "targetName", "entrezGeneId", "targetFamilies", "targetECNumbers",  "targetSynonyms", "dealId") VALUES ($1, $2, $3, $4, $5, $6, $7)`;
          
          db.any(queryString, [drugId, targetName, entrezGeneId, targetFamilies, targetECNumbers, targetSynonyms, dealId])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.dealMapDrugTargets = dealMapDrugTargets;