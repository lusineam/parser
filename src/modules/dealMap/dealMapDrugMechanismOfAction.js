const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function dealMapDrugMechanismOfAction () {
    readCsvFile("../dataCsv/dealMap/drugMechanismOfAction.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let directMechanism = csvData[i]["directMechanism"];
          let HRCHY_1 = csvData[i]["HRCHY_1"];
          let HRCHY_2 = csvData[i]["HRCHY_2"];
          let HRCHY_3 = csvData[i]["HRCHY_3"];
          let HRCHY_4 = csvData[i]["HRCHY_4"];
          let HRCHY_5 = csvData[i]["HRCHY_5"];
          let HRCHY_6 = csvData[i]["HRCHY_6"];
          let dealId = csvData[i]["dealId"];
          let drugPrimaryName = csvData[i]["drugPrimaryName"];

          if (drugId === "" ) {
            drugId = null;
          }
          if (dealId === "" ) {
            dealId = null;
          }
  
          const queryString = `INSERT INTO "dealMapDrugMechanismOfAction" ("drugId", "directMechanism", "HRCHY_1", "HRCHY_2", "HRCHY_3",  "HRCHY_4", "HRCHY_5", "HRCHY_6", "dealId", "drugPrimaryName") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`;
          
          db.any(queryString, [drugId, directMechanism, HRCHY_1, HRCHY_2, HRCHY_3, HRCHY_4, HRCHY_5, HRCHY_6, dealId, drugPrimaryName])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.dealMapDrugMechanismOfAction = dealMapDrugMechanismOfAction;