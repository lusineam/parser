const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function dealMapDealCompany () {
  readCsvFile(`../dataCsv/dealMap/dealCompany.csv`).then((csvData) => {
    for (let i in csvData) {

      let dealId = csvData[i]["dealId"];
      let companyName = csvData[i]["companyName"];
      let companyRoleOnDeal = csvData[i]["companyRoleOnDeal"];
      let companyNameRoleList = csvData[i]["companyNameRoleList"];
      let companyType = csvData[i]["companyType"];
      let companySymbol = csvData[i]["companySymbol"];
      let companyIndustry = csvData[i]["companyIndustry"];

      const queryString = `INSERT INTO "dealMapDealCompany" ("dealId", "companyName", "companyRoleOnDeal", "companyNameRoleList", "companyType", "companySymbol", "companyIndustry") 
          VALUES ($1, $2, $3, $4, $5, $6, $7)`;
      
      db.any(queryString, [dealId, companyName, companyRoleOnDeal, companyNameRoleList, companyType, companySymbol, companyIndustry])
        .then((data) => {})
        .catch((error) => {
          console.log(error, data);
        });
      }
    }).catch((error) => {
      console.log(error, data);
    });
  }

exports.dealMapDealCompany = dealMapDealCompany;