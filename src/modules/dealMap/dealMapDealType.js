const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function dealMapDealType () {
  readCsvFile("../dataCsv/dealMap/dealType.csv").then((csvData) => {
    for (let i in csvData) {

      let dealId = csvData[i]["dealId"];
      let mechanismOfAction = csvData[i]["dealType"];

      const queryString = `INSERT INTO "dealMapDealType" ("dealId", "dealType") VALUES ($1, $2)`;
      
      db.any(queryString, [dealId, mechanismOfAction])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
      }
    }).catch((error) => {
      console.log(error);
  });
}
exports.dealMapDealType = dealMapDealType;
