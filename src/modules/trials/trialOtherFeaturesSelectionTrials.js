const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialOtherFeaturesSelectionTrials () {
    readCsvFile("../dataCsv/trials/OtherFeaturesSelectionTrials.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialPatientSegments = csvData[i]["trialPatientSegments"];
          let trialId = csvData[i]["trialId"];
          let trialDiseases = csvData[i]["trialDiseases"];
          let diseaseId = csvData[i]["Disease_Id"];
         
          
          const queryString = `INSERT INTO "trialOtherFeaturesSelectionTrials" ("trialPatientSegments", "trialId", "trialDiseases", "diseaseId") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialPatientSegments, trialId, trialDiseases, diseaseId])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialOtherFeaturesSelectionTrials = trialOtherFeaturesSelectionTrials;
