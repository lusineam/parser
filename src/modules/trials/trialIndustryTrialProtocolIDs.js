const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustryTrialProtocolIDs () {
    readCsvFile("../dataCsv/trials/industryTrialProtocolIDs.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let trialProtocolIDs = csvData[i]["trialProtocolIDs"];
          let trialIdText = csvData[i]["trialIdText"];
          
          const queryString = `INSERT INTO "trialIndustryTrialProtocolIDs" ("trialId", "trialProtocolIDs", "trialIdText") VALUES ($1, $2, $3)`;
          
          db.any(queryString, [trialId, trialProtocolIDs, trialIdText])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialIndustryTrialProtocolIDs = trialIndustryTrialProtocolIDs;
