const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustryTrialDrugsTested () {
    readCsvFile("../dataCsv/trials/industryTrialDrugsTested.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let drugId = csvData[i]["drugId"];
          let drugNameId = csvData[i]["drugNameId"];
          let drugName = csvData[i]["drugName"];
          let drugRole = csvData[i]["drugRole"];
          let drugList = csvData[i]["drugList"];
          let drugPrimaryName = csvData[i]["drugPrimaryName"];
         
          
          const queryString = `INSERT INTO "trialIndustryTrialDrugsTested" ("trialId", "drugId", "drugNameId", "drugName", "drugRole", "drugList", "drugPrimaryName")
            VALUES ($1, $2, $3, $4, $5, $6, $7)`;
          
          db.any(queryString, [trialId, drugId, drugNameId, drugName, drugRole, drugList, drugPrimaryName])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialIndustryTrialDrugsTested = trialIndustryTrialDrugsTested;