const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustryTrialDrugsTestedAndSponsor () {
    readCsvFile("../dataCsv/trials/industryTrialDrugsTested_and_Sponsor.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let sponsorList = csvData[i]["sponsorList"];
          let drugList = csvData[i]["drugList"];
          let sponsorTrialName = csvData[i]["sponsor_trialname"];
         
          
          const queryString = `INSERT INTO "trialIndustryTrialDrugsTestedAndSponsor" ("trialId", "sponsorList", "drugList", "sponsorTrialName") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialId, sponsorList, drugList, sponsorTrialName])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error, data);
    });
  }
      
  exports.trialIndustryTrialDrugsTestedAndSponsor = trialIndustryTrialDrugsTestedAndSponsor;
