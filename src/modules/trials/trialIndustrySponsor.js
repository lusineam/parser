const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustrySponsor () {
    readCsvFile("../dataCsv/trials/industryTrialSponsors.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let name = csvData[i]["name"];
          let type = csvData[i]["type"];
          let parentCompanyName = csvData[i]["parentCompanyName"];
          let parentCompanyHQCountry = csvData[i]["parentCompanyHQCountry"];
          let parentCompanyHQState = csvData[i]["parentCompanyHQState"];
          let sponsorList = csvData[i]["sponsorList"];
         
          
          const queryString = `INSERT INTO "trialIndustrySponsor" ("trialId", "name", "type", "parentCompanyName", "parentCompanyHQCountry", "parentCompanyHQState", "sponsorList")
            VALUES ($1, $2, $3, $4, $5, $6, $7)`;
          
          db.any(queryString, [trialId, name, type, parentCompanyName, parentCompanyHQCountry, parentCompanyHQState, sponsorList])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialIndustrySponsor = trialIndustrySponsor;