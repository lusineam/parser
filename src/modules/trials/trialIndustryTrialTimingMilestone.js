const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustryTrialTimingMilestone () {
    readCsvFile("../dataCsv/trials/industryTrialTimingMilestone.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let firstResults = csvData[i]["First.results"];
          let USFilingDate = csvData[i]["US.filing.date"];
          let USApprovalDate = csvData[i]["US.approval.date"];
          let EUFilingDate = csvData[i]["EU.filing.date"];
          let EUApprovalDate = csvData[i]["EU.approval.date"];    
          
          if (firstResults === "" ) {
            firstResults = null;
          }
          if (USFilingDate === "" ) {
            USFilingDate = null;
          }
          if (USApprovalDate === "" ) {
            USApprovalDate = null;
          }
          if (EUFilingDate === "" ) {
            EUFilingDate = null;
          }
          if (EUApprovalDate === "" ) {
            EUApprovalDate = null;
          }
          
          const queryString = `INSERT INTO "trialIndustryTrialTimingMilestone" ("trialId", "firstResults", "USFilingDate", "USApprovalDate", "EUFilingDate", "EUApprovalDate")
            VALUES ($1, $2, $3, $4, $5, $6)`;
          
          db.any(queryString, [trialId, firstResults, USFilingDate, USApprovalDate, EUFilingDate, EUApprovalDate])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialIndustryTrialTimingMilestone = trialIndustryTrialTimingMilestone;