const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustryTrialPrimaryEndpoint () {
    readCsvFile("../dataCsv/trials/industryTrialPrimaryEndpoint.csv").then((csvData) => {
        for (let i in csvData) {
        
          let primaryEndpointGroup = csvData[i]["primaryEndpointGroup"];
          let primaryEndpointSubGroup = csvData[i]["primaryEndpointSubGroup"];
          let primaryEndpoint = csvData[i]["primaryEndpoint"];
          let trialId = csvData[i]["trialId"];
         
          
          const queryString = `INSERT INTO "trialIndustryTrialPrimaryEndpoint" ("primaryEndpointGroup", "primaryEndpointSubGroup", "primaryEndpoint", "trialId") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [primaryEndpointGroup, primaryEndpointSubGroup, primaryEndpoint, trialId])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialIndustryTrialPrimaryEndpoint = trialIndustryTrialPrimaryEndpoint;
