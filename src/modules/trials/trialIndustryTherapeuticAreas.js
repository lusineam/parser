const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = [
  "industryTrialTherapeuticAreas_1",
  "industryTrialTherapeuticAreas_2",
  "industryTrialTherapeuticAreas_3",
  "industryTrialTherapeuticAreas_4"
];

function trialIndustryTherapeuticAreas () {
  tables.forEach(async element => {
    await readCsvFile(`../dataCsv/trials/${element}.csv`).then((csvData) => {
      for (let i in csvData) {
      
        let trialId = csvData[i]["trialId"];
        let trialTherapeuticAreas = csvData[i]["trialTherapeuticAreas"];
        let trialDiseases = csvData[i]["trialDiseases"];
        let trialPatientSegments = csvData[i]["trialPatientSegments"];
        let trialPatientSegmentsList = csvData[i]["trialPatientSegments_List"];
        let diseaseId = csvData[i]["Disease_Id"];
        let trialMeshTerms = csvData[i]["trialMeshTerms"];
       
        
        const queryString = `INSERT INTO "trialIndustryTherapeuticAreas" ("trialId", "trialTherapeuticAreas", "trialDiseases", "trialPatientSegments", "trialPatientSegmentsList", "diseaseId", "trialMeshTerms")
          VALUES ($1, $2, $3, $4, $5, $6, $7)`;
        
        db.any(queryString, [trialId, trialTherapeuticAreas, trialDiseases, trialPatientSegments, trialPatientSegmentsList, diseaseId, trialMeshTerms])
          .then((data) => {})
          .catch((error) => {
            console.log(error, data);
          });
        }
    }).catch((error) => {
      console.log(error);
    });
   });
    
  }
      
  exports.trialIndustryTherapeuticAreas = trialIndustryTherapeuticAreas;