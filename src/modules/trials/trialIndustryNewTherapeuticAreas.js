const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = [
  "industryTrialTherapeuticAreas_1",
  "industryTrialTherapeuticAreas_2",
  "industryTrialTherapeuticAreas_3",
  "industryTrialTherapeuticAreas_4"
];

function trialIndustryNewTherapeuticAreas () {
  tables.forEach(async element => {
    await readCsvFile(`../dataCsv/trials/${element}.csv`).then((csvData) => {
      for (let i in csvData) {
      
        let trialId = csvData[i]["trialId"];
        let trialTherapeuticAreas = csvData[i]["trialTherapeuticAreas"];
        let trialDiseases = csvData[i]["trialDiseases"];
        let trialMeshTerms = csvData[i]["trialMeshTerms"];
       
        
        const queryString = `INSERT INTO "trialIndustryNewTherapeuticAreas" ("trialId", "trialTherapeuticAreas", "trialDiseases", "trialMeshTerms")
          VALUES ($1, $2, $3, $4)`;
        
        db.any(queryString, [trialId, trialTherapeuticAreas, trialDiseases, trialMeshTerms])
          .then((data) => {})
          .catch((error) => {
            console.log(error, data);
          });
        }
    }).catch((error) => {
      console.log(error);
    });
   }); 
  }
      
  exports.trialIndustryNewTherapeuticAreas = trialIndustryNewTherapeuticAreas;