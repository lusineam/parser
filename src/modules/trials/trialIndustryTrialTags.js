const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustryTrialTags () {
    readCsvFile("../dataCsv/trials/industryTrialTags.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let trialTags = csvData[i]["trialTags"];
          
          const queryString = `INSERT INTO "trialIndustryTrialTags" ("trialId", "trialTags") VALUES ($1, $2)`;
          
          db.any(queryString, [trialId, trialTags])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialIndustryTrialTags = trialIndustryTrialTags;
