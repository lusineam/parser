const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialPhaseStatusDate () {
    readCsvFile("../dataCsv/trials/industryTrialPanel.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let PBITrialStatus = csvData[i]["PBI_trialStatus"];
          let trialPhase = csvData[i]["trialPhase"];
          let trialStartDate = csvData[i]["trialStartDate"];

          if (trialStartDate === "" ) {
            trialStartDate = null;
          }

          const queryString = `INSERT INTO "trialPhaseStatusDate" ("trialId", "PBITrialStatus", "trialPhase", "trialStartDate") VALUES ($1, $2, $3, $4)`;
          
          db.any(queryString, [trialId, PBITrialStatus, trialPhase, trialStartDate])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialPhaseStatusDate = trialPhaseStatusDate;