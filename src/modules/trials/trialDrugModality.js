const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialDrugModality () {
    readCsvFile("../dataCsv/trials/drugModality.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let drugTherapeuticModality = csvData[i]["drugTherapeuticModality"];
        
          
          const queryString = `INSERT INTO "trialDrugModality" ("drugId", "drugTherapeuticModality") VALUES ($1, $2)`;
          
          db.any(queryString, [drugId, drugTherapeuticModality])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialDrugModality = trialDrugModality;
