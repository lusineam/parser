const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialDrugMechanismOfAction () {
    readCsvFile("../dataCsv/trials/drugMechanismOfAction.csv").then((csvData) => {
        for (let i in csvData) {
        
          let drugId = csvData[i]["drugId"];
          let directMechanism = csvData[i]["directMechanism"];
          let HRCHY1 = csvData[i]["HRCHY_1"];
          let HRCHY2 = csvData[i]["HRCHY_2"];
          let HRCHY3 = csvData[i]["HRCHY_3"];
          let HRCHY4 = csvData[i]["HRCHY_4"];
          let HRCHY5 = csvData[i]["HRCHY_5"];
          let HRCHY6 = csvData[i]["HRCHY_6"];
          let drugPrimaryName = csvData[i]["drugPrimaryName"];
         
          
          const queryString = `INSERT INTO "trialDrugMechanismOfAction" ("drugId", "directMechanism", "HRCHY1", "HRCHY2", "HRCHY3", "HRCHY4", "HRCHY5", "HRCHY6", "drugPrimaryName")
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`;
          
          db.any(queryString, [drugId, directMechanism, HRCHY1, HRCHY2, HRCHY3, HRCHY4, HRCHY5, HRCHY6, drugPrimaryName])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialDrugMechanismOfAction = trialDrugMechanismOfAction;
