const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function trialIndustryCountries () {
    readCsvFile("../dataCsv/trials/industryTrialCountries.csv").then((csvData) => {
        for (let i in csvData) {
        
          let trialId = csvData[i]["trialId"];
          let trialCountries = csvData[i]["trialCountries"];
          let countryCode = csvData[i]["countryCode"];
        
          
          const queryString = `INSERT INTO "trialIndustryCountries" ("trialId", "trialCountries", "countryCode") VALUES ($1, $2, $3)`;
          
          db.any(queryString, [trialId, trialCountries, countryCode])
            .then((data) => {})
            .catch((error) => {
              console.log(error, data);
            });
          }
    }).catch((error) => {
      console.log(error);
    });
  }
      
  exports.trialIndustryCountries = trialIndustryCountries;
