const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergDrugMechanismOfAction() {
  readCsvFile(`../dataCsv/companyMap/drugMechanismOfAction.csv`).then(
    (csvData) => {
      for (let i in csvData) {
        let drugId = csvData[i]["drugId"];
        let directMechanism = csvData[i]["directMechanism"];
        let HRCHY1 = csvData[i]["HRCHY_1"];
        let HRCHY2 = csvData[i]["HRCHY_2"];
        let HRCHY3 = csvData[i]["HRCHY_3"];
        let HRCHY4 = csvData[i]["HRCHY_4"];
        let HRCHY5 = csvData[i]["HRCHY_5"];
        let HRCHY6 = csvData[i]["HRCHY_6"];
        let mechanismSynonyms = csvData[i]["mechanismSynonyms"];

        const queryString = `INSERT INTO "bloombergDrugMechanismOfAction" ("drugId", "directMechanism", "HRCHY1", "HRCHY2", "HRCHY3", "HRCHY4", "HRCHY5", "HRCHY6", "mechanismSynonyms")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`;

        db.any(queryString, [
          drugId,
          directMechanism,
          HRCHY1,
          HRCHY2,
          HRCHY3,
          HRCHY4,
          HRCHY5,
          HRCHY6,
          mechanismSynonyms,
        ])
          .then((data) => {})
          .catch((error) => {
            console.log(error);
          });
      }
    }
  );
}

exports.bloombergDrugMechanismOfAction = bloombergDrugMechanismOfAction;
