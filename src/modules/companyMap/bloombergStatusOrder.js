const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergStatusOrder () {
  readCsvFile(`../dataCsv/companyMap/statusOrder.csv`).then((csvData) => {
    for (let i in csvData) {

      let Group = csvData[i]["Group"];
      let Order = csvData[i]["Order"];

      const queryString = `INSERT INTO "bloombergStatusOrder" ("Group", "Order") VALUES ($1, $2)`;

      db.any(queryString, [Group, Order])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergStatusOrder = bloombergStatusOrder;