const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialTherapeuticAreas () {
  readCsvFile(`../dataCsv/companyMap/industryTrialTherapeuticAreas.csv`).then((csvData) => {
    for (let i in csvData) {

      let trialId = csvData[i]["trialId"];
      let trialTherapeuticAreas = csvData[i]["trialTherapeuticAreas"];
      let trialDiseases = csvData[i]["trialDiseases"];
      let trialPatientSegments = csvData[i]["trialPatientSegments"];
      let indications = csvData[i]["indications"];

      const queryString = `INSERT INTO "bloombergIndustryTrialTherapeuticAreas" ("trialId", "trialTherapeuticAreas", "trialDiseases", "trialPatientSegments", "indications") VALUES ($1, $2, $3, $4, $5)`;

      db.any(queryString, [trialId, trialTherapeuticAreas, trialDiseases, trialPatientSegments, indications])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergIndustryTrialTherapeuticAreas = bloombergIndustryTrialTherapeuticAreas;