const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialOutcomes () {
  readCsvFile(`../dataCsv/companyMap/industryTrialOutcomes.csv`).then((csvData) => {
    for (let i in csvData) {

      let trialId = csvData[i]["trialId"];
      let trialOutcomes = csvData[i]["trialOutcomes"];

      const queryString = `INSERT INTO "bloombergIndustryTrialOutcomes" ("trialId", "trialOutcomes") VALUES ($1, $2)`;

      db.any(queryString, [trialId, trialOutcomes])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergIndustryTrialOutcomes = bloombergIndustryTrialOutcomes;
