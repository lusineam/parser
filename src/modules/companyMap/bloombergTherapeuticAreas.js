const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergTherapeuticAreas () {
  readCsvFile(`../dataCsv/companyMap/drugProgramPanel.csv`).then((csvData) => {
    for (let i in csvData) {

      let therapeuticArea = csvData[i]["therapeuticArea"];
      let diseaseName = csvData[i]["diseaseName"];

      const queryString = `INSERT INTO "bloombergTherapeuticAreas" ("therapeuticArea", "diseaseName") VALUES ($1, $2)`;

      db.any(queryString, [therapeuticArea, diseaseName])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergTherapeuticAreas = bloombergTherapeuticAreas;
