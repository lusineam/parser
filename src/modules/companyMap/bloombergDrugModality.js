const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergDrugModality () {
  readCsvFile(`../dataCsv/companyMap/drugModality.csv`).then((csvData) => {
    for (let i in csvData) {

      let drugId = csvData[i]["drugId"];
      let drugTherapeuticModality = csvData[i]["drugTherapeuticModality"];

      const queryString = `INSERT INTO "bloombergDrugModality" ("drugId", "drugTherapeuticModality") VALUES ($1, $2)`;

      db.any(queryString, [drugId, drugTherapeuticModality])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergDrugModality = bloombergDrugModality;

