const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergFiltersData () {
  readCsvFile(`../dataCsv/companyMap/drugProgramPanel.csv`).then((csvData) => {
    for (let i in csvData) {

      let drugId = csvData[i]["drugId"];
      let countryName = csvData[i]["countryName"];
      let companyName = csvData[i]["companyName"];
      let currentStatus = csvData[i]["currentStatus"];
      let diseaseName = csvData[i]["diseaseName"];
      let therapeuticArea = csvData[i]["therapeuticArea"];

      const queryString = `INSERT INTO "bloombergFiltersData" ("drugId","countryName", "companyName", "currentStatus", "diseaseName", "therapeuticArea") VALUES ($1, $2, $3, $4, $5, $6)`;

      db.any(queryString, [drugId, countryName, companyName, currentStatus, diseaseName, therapeuticArea])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergFiltersData = bloombergFiltersData;