const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergDrugProgramPanel() {
  readCsvFile(`../dataCsv/companyMap/drugProgramPanel.csv`).then((csvData) => {
    for (let i in csvData) {
      let drugRecordUrl = csvData[i]["drugRecordUrl"];
      let drugId = csvData[i]["drugId"];
      let drugPrimaryName = csvData[i]["drugPrimaryName"];
      let countryCode = csvData[i]["countryCode"];
      let countryName = csvData[i]["countryName"];
      let diseaseName = csvData[i]["diseaseName"];
      let companyRelationship = csvData[i]["companyRelationship"];
      let companyName = csvData[i]["companyName"];
      let currentStatus = csvData[i]["currentStatus"];
      let yearLaunch = csvData[i]["yearLaunch"];
      let therapeuticArea = csvData[i]["therapeuticArea"];
      let companyRole = csvData[i]["companyRole"];
      let companyList = csvData[i]["companyList"];
      let licensingOpportunity = csvData[i]["licensingOpportunity"];
      let drugCountryStatus = csvData[i]["drugCountryStatus"];
      let drugCountryYearLaunched = csvData[i]["drugCountryYearLaunched"];
      let targetName = csvData[i]["targetName"];
      let region = csvData[i]["region"];
      let drugDiseaseLeadingPhaseRegional =
        csvData[i]["drugDiseaseLeadingPhaseRegional"];
      let drugDiseaseLeadingPhaseGlobal =
        csvData[i]["drugDiseaseLeadingPhaseGlobal"];
      let drugTargets = csvData[i]["drugTargets"];
      let id1 = csvData[i]["id"];
      let beiGeneTarget = csvData[i]["beiGeneTarget"];
      let drugCompany = csvData[i]["drugCompany"];

      const queryString = `INSERT INTO "bloombergDrugProgramPanel" ("drugRecordUrl", "drugId", "drugPrimaryName", "countryCode", "countryName", "diseaseName", "companyRelationship", "companyName", 
      "currentStatus", "yearLaunch", "therapeuticArea", "companyRole", "companyList", "licensingOpportunity", "drugCountryStatus", "drugCountryYearLaunched", "targetName", "region",
      "drugDiseaseLeadingPhaseRegional", "drugDiseaseLeadingPhaseGlobal", "drugTargets", "id1", "beiGeneTarget", "drugCompany")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24)`;

      db.any(queryString, [
        drugRecordUrl,
        drugId,
        drugPrimaryName,
        countryCode,
        countryName,
        diseaseName,
        companyRelationship,
        companyName,
        currentStatus,
        yearLaunch,
        therapeuticArea,
        companyRole,
        companyList,
        licensingOpportunity,
        drugCountryStatus,
        drugCountryYearLaunched,
        targetName,
        region,
        drugDiseaseLeadingPhaseRegional,
        drugDiseaseLeadingPhaseGlobal,
        drugTargets,
        id1,
        beiGeneTarget,
        drugCompany,
      ])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergDrugProgramPanel = bloombergDrugProgramPanel;
