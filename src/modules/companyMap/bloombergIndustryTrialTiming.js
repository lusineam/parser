const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialTiming() {
  readCsvFile(`../dataCsv/companyMap/industryTrialTiming.csv`).then(
    (csvData) => {
      for (let i in csvData) {
        let trialId = csvData[i]["trialId"];
        let actualTrialStartDate = csvData[i]["actualTrialStartDate"];
        // let trialpredictMinPeakEnrollmentDuration = csvData[i]["trialpredictMinPeakEnrollmentDuration"];
        // let trialpredictMaxPeakEnrollmentDuration = csvData[i]["trialpredictMaxPeakEnrollmentDuration"];
        // let trialpredictMinPeakEnrollmentPeriodCloseDate = csvData[i]["trialpredictMinPeakEnrollmentPeriodCloseDate"];
        // let trialpredictMaxPeakEnrollmentPeriodCloseDate =
        //   csvData[i]["trialpredictMaxPeakEnrollmentPeriodCloseDate"];
        // let trialpredictMinPeakTreatmentDuration = csvData[i]["trialpredictMinPeakTreatmentDuration"];
        // let trialpredictMaxPeakTreatmentDuration = csvData[i]["trialpredictMaxPeakTreatmentDuration"];
        // let trialpredictMinPeakTreatmentPeriodEndDate = csvData[i]["trialpredictMinPeakTreatmentPeriodEndDate"];
        // let trialpredictMaxPeakTreatmentPeriodEndDate = csvData[i]["trialpredictMaxPeakTreatmentPeriodEndDate"];
        let actualPrimaryCompletionDate =
          csvData[i]["actualPrimaryCompletionDate"];
        let totalTrialDuration = csvData[i]["totalTrialDuration"];
        let actualTotalTrialCloseDate = csvData[i]["actualTotalTrialCloseDate"];
        let actualPrimaryEndpointsReported =
          csvData[i]["actualPrimaryEndpointsReported"];
        // let trialpredictMinPeakTotalTrialDuration = csvData[i]["trialpredictMinPeakTotalTrialDuration"];
        // let trialpredictMaxPeakTotalTrialDuration = csvData[i]["trialpredictMaxPeakTotalTrialDuration"];
        // let trialpredictMinTotalTrialCloseDate = csvData[i]["trialpredictMinTotalTrialCloseDate"];
        // let trialpredictMaxTotalTrialCloseDate = csvData[i]["trialpredictMaxTotalTrialCloseDate"];
        // let predictedTrialEnrollmentDuration = csvData[i]["predictedTrialEnrollmentDuration"];
        // let predictedEnrollmentPeriodCloseDate = csvData[i]["predictedEnrollmentPeriodCloseDate"];
        // let predictedTreatmentDuration = csvData[i]["predictedTreatmentDuration"];
        // let predictedPrimaryCompletionDate = csvData[i]["predictedPrimaryCompletionDate"];
        // let predictedTotalTrialDuration = csvData[i]["predictedTotalTrialDuration"];
        let anticipatedEnrollmentPeriodCloseDate =
          csvData[i]["anticipatedEnrollmentPeriodCloseDate"];
        let anticipatedPrimaryCompletionDate =
          csvData[i]["anticipatedPrimaryCompletionDate"];
        let anticipatedTotalTrialCloseDate =
          csvData[i]["anticipatedTotalTrialCloseDate"];
        let actualEnrollmentDuration = csvData[i]["actualEnrollmentDuration"];
        let actualEnrollmentPeriodCloseDate =
          csvData[i]["actualEnrollmentPeriodCloseDate"];
        let actualTreatmentDuration = csvData[i]["actualTreatmentDuration"];
        let anticipatedEnrollmentDuration =
          csvData[i]["anticipatedEnrollmentDuration"];
        let anticipatedTreatmentDuration =
          csvData[i]["anticipatedTreatmentDuration"];
        let anticipatedTrialStartDate = csvData[i]["anticipatedTrialStartDate"];
        let anticipatedPrimaryEndpointsReported =
          csvData[i]["anticipatedPrimaryEndpointsReported"];
        let trialStartDate = csvData[i]["trialStartDate"];
        let trialEndDate = csvData[i]["trialEndDate"];
        let trialPrimaryEndpointsReported =
          csvData[i]["trialPrimaryEndpointsReported"];
        let trialEnrollmentDurationDays =
          csvData[i]["trialEnrollmentDurationDays"];

        if (trialStartDate === "") {
          trialStartDate = null;
        }
        if (trialEndDate === "") {
          trialEndDate = null;
        }

        const queryString = `INSERT INTO "bloombergIndustryTrialTiming" ("trialId", "actualTrialStartDate", "actualPrimaryCompletionDate", "totalTrialDuration",
        "actualTotalTrialCloseDate", "actualPrimaryEndpointsReported", "anticipatedEnrollmentPeriodCloseDate", "anticipatedPrimaryCompletionDate", "anticipatedTotalTrialCloseDate", "actualEnrollmentDuration",
        "actualEnrollmentPeriodCloseDate", "actualTreatmentDuration", "anticipatedEnrollmentDuration", "anticipatedTreatmentDuration", "anticipatedTrialStartDate", "anticipatedPrimaryEndpointsReported",
      "trialStartDate", "trialEndDate", "trialPrimaryEndpointsReported", "trialEnrollmentDurationDays")
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20)`;

        db.any(queryString, [
          trialId,
          actualTrialStartDate,
          actualPrimaryCompletionDate,
          totalTrialDuration,
          actualTotalTrialCloseDate,
          actualPrimaryEndpointsReported,
          anticipatedEnrollmentPeriodCloseDate,
          anticipatedPrimaryCompletionDate,
          anticipatedTotalTrialCloseDate,
          actualEnrollmentDuration,
          actualEnrollmentPeriodCloseDate,
          actualTreatmentDuration,
          anticipatedEnrollmentDuration,
          anticipatedTreatmentDuration,
          anticipatedTrialStartDate,
          anticipatedPrimaryEndpointsReported,
          trialStartDate,
          trialEndDate,
          trialPrimaryEndpointsReported,
          trialEnrollmentDurationDays,
        ])
          .then((data) => {})
          .catch((error) => {
            console.log(error, data);
          });
      }
    }
  );
}

exports.bloombergIndustryTrialTiming = bloombergIndustryTrialTiming;
