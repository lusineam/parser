const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergOrganizationGeoLocation () {
  readCsvFile(`../dataCsv/companyMap/organizationGeoLocation.csv`).then((csvData) => {
    for (let i in csvData) {

      let organizationId = csvData[i]["organizationId"];
      let lat = csvData[i]["lat"];
      let lon = csvData[i]["lon"];

      const queryString = `INSERT INTO "bloombergOrganizationGeoLocation" ("organizationId", "lat", "lon") VALUES ($1, $2, $3)`;

      db.any(queryString, [organizationId, lat, lon])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergOrganizationGeoLocation = bloombergOrganizationGeoLocation;
