const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialProtocolIDs () {
  readCsvFile(`../dataCsv/companyMap/industryTrialProtocolIDs.csv`).then((csvData) => {
    for (let i in csvData) {

      let trialId = csvData[i]["trialId"];
      let trialProtocolIDs = csvData[i]["trialProtocolIDs"];
      let trialIdText = csvData[i]["trialIdText"];

      const queryString = `INSERT INTO "bloombergIndustryTrialProtocolIDs" ("trialId", "trialProtocolIDs", "trialIdText") VALUES ($1, $2, $3)`;

      db.any(queryString, [trialId, trialProtocolIDs, trialIdText])
        .then((data) => {})
        .catch((error) => {
          console.log(error, data);
        });
    }
  });
}

exports.bloombergIndustryTrialProtocolIDs = bloombergIndustryTrialProtocolIDs;