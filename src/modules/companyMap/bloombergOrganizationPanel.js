const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergOrganizationPanel () {
  readCsvFile(`../dataCsv/companyMap/organizationPanel.csv`).then((csvData) => {
    for (let i in csvData) {

      let recordUrl = csvData[i]["recordUrl"];
      let organizationId = csvData[i]["organizationId"];
      let organizationName = csvData[i]["organizationName"];
      let organizationType = csvData[i]["organizationType"];
      let organizationLastTrialStartDate = csvData[i]["organizationLastTrialStartDate"];
      let organizationTotalTrials = csvData[i]["organizationTotalTrials"];
      let organizationTotalOngoingTrials = csvData[i]["organizationTotalOngoingTrials"];
      let organizationTotalPastTrials = csvData[i]["organizationTotalPastTrials"];
      let organizationCountryName = csvData[i]["organizationCountryName"];
      let updatedDate = csvData[i]["updatedDate"];

      const queryString = `INSERT INTO "bloombergOrganizationPanel" ("recordUrl", "organizationId", "organizationName", "organizationType", "organizationLastTrialStartDate", "organizationTotalTrials",
      "organizationTotalOngoingTrials", "organizationTotalPastTrials", "organizationCountryName", "updatedDate") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`;

      db.any(queryString, [recordUrl, organizationId, organizationName, organizationType, organizationLastTrialStartDate, organizationTotalTrials, organizationTotalOngoingTrials, organizationTotalPastTrials,
        organizationCountryName, updatedDate])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergOrganizationPanel = bloombergOrganizationPanel;