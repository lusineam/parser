const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergDrugPanel () {
  readCsvFile(`../dataCsv/companyMap/drugPanel.csv`).then((csvData) => {
    for (let i in csvData) {

      let recordUrl = csvData[i]["recordUrl"];
      let drugId = csvData[i]["drugId"];
      let drugPrimaryName = csvData[i]["drugPrimaryName"];
      let overview = csvData[i]["overview"];
      let marketing = csvData[i]["marketing"];
      let licensing = csvData[i]["licensing"];
      let phaseII = csvData[i]["phaseII"];
      let phaseIII = csvData[i]["phaseIII"];
      let preClinical = csvData[i]["preClinical"];
      let globalStatus = csvData[i]["globalStatus"];
      let developmentStatus = csvData[i]["developmentStatus"];
      let originatorName = csvData[i]["originatorName"];
      let originatorStatus = csvData[i]["originatorStatus"];
      let originatorCompanyKey = csvData[i]["originatorCompanyKey"];
      let latestChangeDate = csvData[i]["latestChangeDate"];
      let latestChange = csvData[i]["latestChange"];
      let origin = csvData[i]["origin"];
      let isPharmaProjectsDrug = csvData[i]["isPharmaProjectsDrug"];
      let drugSource = csvData[i]["drugSource"];
      let originatorCountry = csvData[i]["originatorCountry"];
      let updatedDate = csvData[i]["updatedDate"];
      let molecularFormula = csvData[i]["molecularFormula"];
      let molecularWeight = csvData[i]["molecularWeight"];
      let nce = csvData[i]["nce"];
      let originatorParentName = csvData[i]["originatorParentName"];
      let originatorParentKey = csvData[i]["originatorParentKey"];
      let phaseI = csvData[i]["phaseI"];

      const queryString = `INSERT INTO "bloombergDrugPanel" ("recordUrl", "drugId", "drugPrimaryName", "overview", "marketing", "licensing", "phaseII", "phaseIII", "preClinical",
        "globalStatus", "developmentStatus", "originatorName", "originatorStatus", "originatorCompanyKey", "latestChangeDate", "latestChange", "origin", "isPharmaProjectsDrug", "drugSource", "originatorCountry",
        "updatedDate", "molecularFormula", "molecularWeight", "nce", "originatorParentName", "originatorParentKey", "phaseI")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27)`;

      db.any(queryString, [recordUrl, drugId, drugPrimaryName, overview, marketing, licensing, phaseII, phaseIII, preClinical, globalStatus, developmentStatus, originatorName, originatorStatus, 
        originatorCompanyKey, latestChangeDate, latestChange, origin, isPharmaProjectsDrug, drugSource, originatorCountry, updatedDate, molecularFormula, molecularWeight, nce,
        originatorParentName, originatorParentKey, phaseI])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergDrugPanel = bloombergDrugPanel;