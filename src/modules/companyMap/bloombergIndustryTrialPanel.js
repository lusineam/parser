const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialPanel() {
  readCsvFile(`../dataCsv/companyMap/industryTrialPanel.csv`)
    .then((csvData) => {
      for (let i in csvData) {
        let recordUrl = csvData[i]["recordUrl"];
        let trialId = csvData[i]["trialId"];
        let trialTitle = csvData[i]["trialTitle"];
        let trialStatus = csvData[i]["trialStatus"];
        let trialPhase = csvData[i]["trialPhase"];
        let trialStartDate = csvData[i]["trialStartDate"];
        let trialPrimaryCompletionDate =
          csvData[i]["trialPrimaryCompletionDate"];
        let trialPrimaryEndpointsReported =
          csvData[i]["trialPrimaryEndpointsReported"];
        let trialObjective = csvData[i]["trialObjective"];
        let trialInclusionCriteria = csvData[i]["trialInclusionCriteria"];
        let trialExclusionCriteria = csvData[i]["trialExclusionCriteria"];
        let trialStudyDesign = csvData[i]["trialStudyDesign"];
        let trialPatientPopulation = csvData[i]["trialPatientPopulation"];
        let trialTreatmentPlan = csvData[i]["trialTreatmentPlan"];
        let trialPriorConcurrentTherapy =
          csvData[i]["trialPriorConcurrentTherapy"];
        let trialLastModifiedDate = csvData[i]["trialLastModifiedDate"];
        let trialTargetAccrual = csvData[i]["trialTargetAccrual"];
        let trialTargetAccrualText = csvData[i]["trialTargetAccrualText"];
        let trialActualAccrual = csvData[i]["trialActualAccrual"];
        let trialActualAccrualText = csvData[i]["trialActualAccrualText"];
        let trialIdentifiedSites = csvData[i]["trialIdentifiedSites"];
        let trialRecordType = csvData[i]["trialRecordType"];
        let updatedDate = csvData[i]["updatedDate"];
        let trialOutcomeDetails = csvData[i]["trialOutcomeDetails"];
        let trialReportedSites = csvData[i]["trialReportedSites"];
        let trialPatientsPerSitePerMonth =
          csvData[i]["trialPatientsPerSitePerMonth"];

        if (trialStartDate === "") {
          trialStartDate = null;
        }
        if (trialPrimaryCompletionDate === "") {
          trialPrimaryCompletionDate = null;
        }
        if (trialLastModifiedDate === "") {
          trialLastModifiedDate = null;
        }
        if (updatedDate === "") {
          updatedDate = null;
        }

        const queryString = `INSERT INTO "bloombergIndustryTrialPanel" ("recordUrl", "trialId", "trialTitle", "trialStatus", "trialPhase", "trialStartDate", "trialPrimaryCompletionDate", "trialPrimaryEndpointsReported",
       "trialObjective", "trialInclusionCriteria", "trialExclusionCriteria", "trialStudyDesign", "trialPatientPopulation", "trialTreatmentPlan", "trialPriorConcurrentTherapy", "trialLastModifiedDate", 
        "trialTargetAccrual", "trialTargetAccrualText", "trialActualAccrual", "trialActualAccrualText",
        "trialIdentifiedSites", "trialRecordType", "updatedDate", "trialOutcomeDetails", "trialReportedSites", "trialPatientsPerSitePerMonth")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26)`;

        db.any(queryString, [
          recordUrl,
          trialId,
          trialTitle,
          trialStatus,
          trialPhase,
          trialStartDate,
          trialPrimaryCompletionDate,
          trialPrimaryEndpointsReported,
          trialObjective,
          trialInclusionCriteria,
          trialExclusionCriteria,
          trialStudyDesign,
          trialPatientPopulation,
          trialTreatmentPlan,
          trialPriorConcurrentTherapy,
          trialLastModifiedDate,
          trialTargetAccrual,
          trialTargetAccrualText,
          trialActualAccrual,
          trialActualAccrualText,
          trialIdentifiedSites,
          trialRecordType,
          updatedDate,
          trialOutcomeDetails,
          trialReportedSites,
          trialPatientsPerSitePerMonth,
        ])
          .then((data) => {})
          .catch((error) => {
            console.log(error, data);
          });
      }
    })
    .catch((error) => {
      console.log(error);
    });
}

exports.bloombergIndustryTrialPanel = bloombergIndustryTrialPanel;
