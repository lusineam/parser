const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialSponsors() {
  readCsvFile(`../dataCsv/companyMap/industryTrialSponsors.csv`).then(
    (csvData) => {
      for (let i in csvData) {
        let trialId = csvData[i]["trialId"];
        let name = csvData[i]["name"];
        let type = csvData[i]["type"];
        let parentCompanyName = csvData[i]["parentCompanyName"];
        let parentCompanyHQCountry = csvData[i]["parentCompanyHQCountry"];
        let parentCompanyHQState = csvData[i]["parentCompanyHQState"];
        let sponsorList = csvData[i]["sponsorList"];
        let parentCompanyHQCity = csvData[i]["parentCompanyHQCity"];
        let parentCompanyHQPostCode = csvData[i]["parentCompanyHQPostCode"];

        const queryString = `INSERT INTO "bloombergIndustryTrialSponsors" ("trialId", "name", "type", "parentCompanyName", "parentCompanyHQCountry", "parentCompanyHQState", "sponsorList", "parentCompanyHQCity", "parentCompanyHQPostCode")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`;

        db.any(queryString, [
          trialId,
          name,
          type,
          parentCompanyName,
          parentCompanyHQCountry,
          parentCompanyHQState,
          sponsorList,
          parentCompanyHQCity,
          parentCompanyHQPostCode,
        ])
          .then((data) => {})
          .catch((error) => {
            console.log(error);
          });
      }
    }
  );
}

exports.bloombergIndustryTrialSponsors = bloombergIndustryTrialSponsors;
