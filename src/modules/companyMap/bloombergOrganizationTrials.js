const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

const tables = ["organizationTrials_1", "organizationTrials_2"];

function bloombergOrganizationTrials() {
  tables.forEach(async (element) => {
    await readCsvFile(`../dataCsv/companyMap/${element}.csv`)
      .then((csvData) => {
        for (let i in csvData) {
          let organizationId = csvData[i]["organizationId"];
          let trialId = csvData[i]["trialId"];
          let trialStatus = csvData[i]["trialStatus"];
          let trialStartDate = csvData[i]["trialStartDate"];
          let trialPhase = csvData[i]["trialPhase"];

          const queryString = `INSERT INTO "bloombergOrganizationTrials" ("organizationId", "trialId", "trialStatus", "trialStartDate", "trialPhase") VALUES ($1, $2, $3, $4, $5)`;

          db.any(queryString, [
            organizationId,
            trialId,
            trialStatus,
            trialStartDate,
            trialPhase,
          ])
            .then((data) => {})
            .catch((error) => {
              console.log(error);
            });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  });
}

exports.bloombergOrganizationTrials = bloombergOrganizationTrials;
