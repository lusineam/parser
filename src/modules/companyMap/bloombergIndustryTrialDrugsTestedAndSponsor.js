const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialDrugsTestedAndSponsor() {
  readCsvFile(
    `../dataCsv/companyMap/industryTrialDrugsTested_and_Sponsor.csv`
  ).then((csvData) => {
    for (let i in csvData) {
      let trialId = csvData[i]["trialId"];
      let drugId = csvData[i]["drugId"];
      let drugNameId = csvData[i]["drugNameId"];
      let drugName = csvData[i]["drugName"];
      let drugRole = csvData[i]["drugRole"];
      let drugList = csvData[i]["drugList"];
      let name = csvData[i]["name"];
      let type = csvData[i]["type"];
      let parentCompanyName = csvData[i]["parentCompanyName"];
      let parentCompanyHQCountry = csvData[i]["parentCompanyHQCountry"];
      let parentCompanyHQState = csvData[i]["parentCompanyHQState"];
      let sponsorList = csvData[i]["sponsorList"];
      let drugCompany = csvData[i]["drug_company"];
      let drugPrimaryName = csvData[i]["drugPrimaryName"];
      let parentCompanyHQCity = csvData[i]["parentCompanyHQCity"];
      let parentCompanyHQPostCode = csvData[i]["parentCompanyHQPostCode"];

      const queryString = `INSERT INTO "bloombergIndustryTrialDrugsTestedAndSponsor" ("trialId", "drugId", "drugNameId", "drugName", "drugRole", "drugList", "name", "type", "parentCompanyName", "parentCompanyHQCountry", 
      "parentCompanyHQState", "sponsorList", "drugCompany", "drugPrimaryName", "parentCompanyHQCity", "parentCompanyHQPostCode") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16)`;

      db.any(queryString, [
        trialId,
        drugId,
        drugNameId,
        drugName,
        drugRole,
        drugList,
        name,
        type,
        parentCompanyName,
        parentCompanyHQCountry,
        parentCompanyHQState,
        sponsorList,
        drugCompany,
        drugPrimaryName,
        parentCompanyHQCity,
        parentCompanyHQPostCode,
      ])
        .then((data) => {})
        .catch((error) => {
          console.log(error);
        });
    }
  });
}

exports.bloombergIndustryTrialDrugsTestedAndSponsor =
  bloombergIndustryTrialDrugsTestedAndSponsor;
