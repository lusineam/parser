const readCsvFile = require("../../readCsvFile").readCsvFile;
const db = require("../../connection").db;

function bloombergIndustryTrialDrugsTested() {
  readCsvFile(`../dataCsv/companyMap/industryTrialDrugsTested.csv`).then(
    (csvData) => {
      for (let i in csvData) {
        let drugId = csvData[i]["drugId"];
        let drugList = csvData[i]["drugList"];
        let drugName = csvData[i]["drugName"];
        let drugNameId = csvData[i]["drugNameId"];
        let drugPrimaryName = csvData[i]["drugPrimaryName"];
        let drugRole = csvData[i]["drugRole"];
        let trialId = csvData[i]["trialId"];

        const queryString = `INSERT INTO "bloombergIndustryTrialDrugsTested" ("drugId", "drugList", "drugName", "drugNameId", "drugPrimaryName", "drugRole", "trialId") VALUES ($1, $2, $3, $4, $5, $6, $7)`;

        db.any(queryString, [
          drugId,
          drugList,
          drugName,
          drugNameId,
          drugPrimaryName,
          drugRole,
          trialId,
        ])
          .then((data) => {})
          .catch((error) => {
            console.log(error);
          });
      }
    }
  );
}

exports.bloombergIndustryTrialDrugsTested = bloombergIndustryTrialDrugsTested;
