const csvParser = require("csv-parser");
const fs = require("fs");

const readCsvFile = async function (filePath) {
  let csvData = [];
  const csvReadStream = fs.createReadStream(filePath).pipe(csvParser());
  for await (data of csvReadStream) {
    csvData.push(data);
  }
  // csvData.pop();
  return csvData;
};

exports.readCsvFile = readCsvFile;
