const promise = require("bluebird");
const initOptions = { promiseLib: promise };
const pgp = require("pg-promise")(initOptions);

const connection = {
  host: "localhost",
  port: 5432,
  database: "updatedata",
  user: "postgres",
  password: "postgres",
};

const db = pgp(connection);

exports.db = db;
